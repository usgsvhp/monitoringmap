// THANKS: https://css-tricks.com/gulp-for-beginners/

var gulp = require('gulp');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var del = require('del');
var htmlmin = require('gulp-htmlmin');
var runSequence = require('run-sequence');
var rename = require("gulp-rename");
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

gulp.task('clean:dist', function() {
    return del.sync('./dist');
});

gulp.task('htmlmin', function() {
    return gulp.src('app/templates/*.html')
        .pipe(htmlmin({
                    collapseWhitespace: true,
                    minifyCSS: true,
                    minifyJS: {compress: {drop_console: true}},
                    processConditionalComments: true,
                    removeComments: true,
                    removeEmptyAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true
            }))
        .pipe(gulp.dest('./dist/templates'));
});

gulp.task('cssjsmin', function(){
    return gulp.src('app/index.html')
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify()))
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('./dist'));
});

gulp.task('moveindex', function() {
    return gulp.src("./app/index-dist.html")
        .pipe(rename("index.html"))
        .pipe(gulp.dest("./dist"));
});

gulp.task('moveicon', function() {
    return gulp.src("./app/favicon.ico")
        .pipe(gulp.dest("./dist"));
});

gulp.task('movejson', function() {
    return gulp.src("./app/scripts/json/*.json")
            .pipe(gulp.dest("./dist/scripts/json"));
});

gulp.task('build', function(callback) {
    return runSequence('clean:dist', 'htmlmin', 'cssjsmin', 'moveindex', 'moveicon', 'movejson');
});

gulp.task('serve', function(callback) {
    runSequence('clean:dist', 'htmlmin', 'cssjsmin', 'moveindex', 'moveicon', 'movejson', () => {
        browserSync.init({
            notify: false,
            port: 9000,
            server: { baseDir: ['app'] }
        });

        gulp.watch('app/*.html').on('change', reload);
        gulp.watch('app/styles/**/*.css').on('change', reload);
        gulp.watch('app/scripts/**/*.js').on('change', reload);
    });
});

gulp.task('serve:dist', () => {
    runSequence('clean:dist', 'htmlmin', 'cssjsmin', 'moveindex', 'moveicon', 'movejson', () => {
        browserSync.init({
            notify: false,
            port: 9000,
            server: { baseDir: ['dist'] }
        });
    });
});
