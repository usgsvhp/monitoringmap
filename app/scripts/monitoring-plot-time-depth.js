// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
(function (monPlotTimeDepth, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var isInitialized = false, dateTicks = 3,
        maxMag = -99.0, minMag = 99.0, maxDepth = -99.0, minDepth = 99.0, minTime = 0, maxTime = 0; // boundary values

    var chart = {
              'PADT': 5, 'PADR': 5, 'PADB': 20, 'PADL': 40, 'canvasH': 0, 'canvasW': 0, 'canvas': {}, 'context': {}
            };
        
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    function renderTimeDepth() {

        monQuakeMap.deactivateQuake();
        monQuakeList.tablerowInactive();
        
        chart.context.clearRect(0, 0, chart.canvasW, chart.canvasH); // Empty the canvas.
    
        var pxPerKm = 0.0, 
            pxPerMs = (chart.canvasW - chart.PADL - chart.PADR) / (maxTime - minTime); // Pixel width / millisecond range

        monPlotChart.renderChartBox(chart); // Render box around grid top, right, bottom, left
        
        // Render x axis date labels and vertical date lines
        if (persistentOptions.get('utcOrLocal') === 'UTC') {
            
            for (
                var idx = (minTime + monUtils.getUTCOffsetMs()); 
                idx <= (maxTime + monUtils.getUTCOffsetMs()); 
                idx += (monUtils.MSDAY * dateTicks)
            ) {

                var pxX = ((idx - minTime) * pxPerMs) + chart.PADL;
                if (pxX < chart.PADL)  { continue; }
                monPlotChart.drawLine(chart.context, pxX, chart.PADT, pxX, chart.canvasH - chart.PADB); // Vertical line
                monPlotChart.drawText(
                        chart.context, monUtils.dateYMD(new Date(idx), 'UTC'), pxX, chart.canvasH - chart.PADB + 15, 'center'
                        );
            }
            
        } else if (persistentOptions.get('utcOrLocal') === 'local') {
            
            for (var idx = minTime; idx <= maxTime; idx += (monUtils.MSDAY * dateTicks)) {
                
                var pxX = ((idx - minTime) * pxPerMs) + chart.PADL;
                if (pxX < chart.PADL)  { continue; }
                monPlotChart.drawLine(chart.context, pxX, chart.PADT, pxX, chart.canvasH - chart.PADB); // Vertical line
                monPlotChart.drawText(
                        chart.context, monUtils.dateYMD(new Date(idx), 'local'), pxX, chart.canvasH - chart.PADB + 15, 'center'
                        );
            }
        }

        // DEPTHS
        // Because depths can be negative this gets complicated, negative depths are above sea level.
        var maxDepthAbs = 0.0, depthOffset = 0.0;
        if (maxDepth < 0) { // All depths above sea level
            maxDepthAbs = Math.ceil(Math.abs(maxDepth));
            depthOffset = Math.abs(Math.floor(minDepth));
        } else if (minDepth < 0 && maxDepth > 0) { // Mix of depths
            maxDepthAbs = Math.abs(Math.floor(minDepth)) + Math.ceil(maxDepth);
            depthOffset = Math.abs(Math.floor(minDepth));
        } else if (minDepth >= 0) { // All depths below sea level.
            maxDepthAbs = Math.ceil(maxDepth);
            depthOffset = 0.0;
        } else {
            // console.log('ERROR: none of the above, minDepth: ' + minDepth + ' - maxDepth: ' + maxDepth);
        }
        pxPerKm = (chart.canvasH - chart.PADT - chart.PADB) / maxDepthAbs;

        // Render y axis depth labels and horizontal lines
        for (var idx = Math.floor(minDepth); idx <= Math.ceil(maxDepth); idx += 2) {
            
            var pxY = ((idx + depthOffset) * pxPerKm) + chart.PADT;
            monPlotChart.drawLine(chart.context, chart.PADL, pxY, chart.canvasW - chart.PADR, pxY); // Horizontal depth lines.
            
            var depthString = monUtils.round(monUtils.kmToMi(idx), 1) + ' mi';
            if (persistentOptions.get('kmOrMi') === 'km') { depthString = idx + ' km'; }
            
            monPlotChart.drawText(chart.context, depthString, chart.PADL - 4, pxY + 6, 'right');
        }

        // Render quakes in plot.
        monPlotChart.needsQuakeLabel = false;
        for(var idx = 0; idx < monPlotChart.quakesInPolygon.length; idx++) {

            var quakeRec = monPlotChart.quakesInPolygon[idx];
            if (quakeRec.isFiltered) { continue; }
            var pxX = ((quakeRec.date.getTime() - minTime) * pxPerMs) + chart.PADL;
            var pxY = ((parseFloat(quakeRec.depthKM) + depthOffset) * pxPerKm) + chart.PADT;
            monPlotChart.drawQuakeCircle(chart.context, quakeRec, pxX, pxY);
        }
        if (monPlotChart.needsQuakeLabel) { monPlotChart.drawQuakeLabel(chart.context); }
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monPlotTimeDepth.renderTimeDepth = function() {
        
        if (!isInitialized) {

            isInitialized = true;

            chart.canvas = document.getElementById('timeDepthPlotCanvas');
            chart.context = chart.canvas.getContext('2d');
            chart.context.translate(0.5, 0.5); // Fixes issues with fuzzy lines.
            chart.canvasW = chart.canvas.width;
            chart.canvasH = chart.canvas.height;

            // Track canvas mouse position.
            chart.canvas.addEventListener('mousemove', function (evt) {
                var rect = chart.canvas.getBoundingClientRect();
                monPlotChart.mousePosition = {
                    x: evt.clientX - rect.left,
                    y: evt.clientY - rect.top
                };
                renderTimeDepth();
            }, false);
        }
        chart.context.clearRect(0, 0, chart.canvasW, chart.canvasH); // Empty the canvas.
        
        // determine boundary values;
        maxMag = -999.0;
        minMag = 999.0;
        maxDepth = -999.0;
        minDepth = 999.0;
        minTime = 0;
        maxTime = 0;
                
        for(var idx = 0; idx < monPlotChart.quakesInPolygon.length; idx++) {
            
            var quakeRec = monPlotChart.quakesInPolygon[idx];
            if (quakeRec.isFiltered) { continue; }
            // Magnitude 
            if (maxMag < parseFloat(quakeRec.mag)) { maxMag = parseFloat(quakeRec.mag); }
            if (minMag > parseFloat(quakeRec.mag)) { minMag = parseFloat(quakeRec.mag); }
            // Depth KM
            if (maxDepth < parseFloat(quakeRec.depthKM)) { maxDepth = parseFloat(quakeRec.depthKM); }
            if (minDepth > parseFloat(quakeRec.depthKM)) { minDepth = parseFloat(quakeRec.depthKM); }
            // Time (use unix time value, e.g. 1489887407527 ms from 1970.)
            if (maxTime === 0) { maxTime = quakeRec.date.getTime(); }
            if (minTime === 0) {
                minTime = quakeRec.date.getTime();
            }
            if (maxTime < quakeRec.date.getTime()) { maxTime = quakeRec.date.getTime(); }
            if (minTime > quakeRec.date.getTime()) {
                minTime = quakeRec.date.getTime();
            }
        }

        // Remove time components from date values.
        minTime = monUtils.removeTime(new Date(minTime)).getTime();
        maxTime = monUtils.removeTime(monUtils.addDays(new Date(maxTime), 1)).getTime();
        
        dateTicks = monUtils.dateDiffDD(new Date(maxTime), new Date(minTime));
        if (dateTicks > 5) { dateTicks = 5; }
        
        renderTimeDepth();
    };
    
}(window.monPlotTimeDepth = window.monPlotTimeDepth || {}, jQuery));
