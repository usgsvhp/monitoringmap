// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
/* global monUiOptions */
(function (monInstrumentUI, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var visibleTotal = 0, modalInstRec = {}, categoryOptions = {};
    
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function refreshUI() {

        var visibleHtml = visibleTotal + ' Instruments Visible (of ' + monInstrumentData.instrumentRecs.length + ')';
        if (visibleTotal === 1) {
            visibleHtml = visibleTotal + ' Instrument Visible (of ' + monInstrumentData.instrumentRecs.length + ')';
        }
        if (monUiOptions.mapZoom < monUiOptions.zoomThreshold) {
            visibleHtml = '0 Instruments Visible (of ' + monInstrumentData.instrumentRecs.length + ')' +
                        '<br/><br/><i>Zoom in to view instruments.</i>';
        }
        $('#vscmmInstHeading').find('a').html(visibleHtml);
        if (monUiOptions.mapZoom < monUiOptions.zoomThreshold) { return; }
        
        if (monMain.isDesktop) {
            if (visibleTotal === monInstrumentData.instrumentRecs.length) {
                $('.vscmm-btn-inst-sh-show').slideUp();
            } else {
                $('.vscmm-btn-inst-sh-show').slideDown();
            }
            if (visibleTotal === 0) {
                $('.vscmm-btn-inst-sh-hide').slideUp();
            } else {
                $('.vscmm-btn-inst-sh-hide').slideDown();
            }
        } else {
            if (visibleTotal > 0) {
                $('.vscmm-btn-inst-sh-show').hide();
                $('.vscmm-btn-inst-sh-hide').show();
            } else {
                $('.vscmm-btn-inst-sh-show').show();
                $('.vscmm-btn-inst-sh-hide').hide();
            }
        }
        
        monLegend.renderInstCats();
    }

    function hideCategory(catId) {
        
        var catRec = monInstrumentData.getCategory(catId);
        visibleTotal -= catRec.categoryTotal;
        categoryOptions[catId] = 'hidden';
        persistentOptions.put('categoryOptions', categoryOptions);
        $('.vscmm-btn-category[data-category-id="' + catId + '"]').removeClass('btn-primary');
        $('.vscmm-btn-category[data-category-id="' + catId + '"]').addClass('btn-default');
        monInstMap.refreshInstruments();
        
        refreshUI();
    }
    
    function showCategory(catId) {
        
        var catRec = monInstrumentData.getCategory(catId);
        visibleTotal += catRec.categoryTotal;
        categoryOptions[catId] = 'visible';
        persistentOptions.put('categoryOptions', categoryOptions);
        $('.vscmm-btn-category[data-category-id="' + catId + '"]').removeClass('btn-default');
        $('.vscmm-btn-category[data-category-id="' + catId + '"]').addClass('btn-primary');
        monInstMap.refreshInstruments();
        
        refreshUI();
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////
    
    monInstrumentUI.getVisibleTotal = function() { return visibleTotal; };
    
    monInstrumentUI.isCategoryVisible = function(catId) {
    
        return $('.vscmm-btn-category[data-category-id="' + catId + '"]').hasClass('btn-primary');
    };
    
    monInstrumentUI.showHideAll = function(isVisible) { // isVisible is boolean: true to show all, else false.

        categoryOptions = {};
        for (var idx = 0; idx < monInstrumentData.instrumentCategories.length; idx++) {
            var catId = monInstrumentData.instrumentCategories[idx].catId;
            categoryOptions[catId] = (isVisible ? 'visible' : 'hidden');
        }
        persistentOptions.put('categoryOptions', categoryOptions);
        monInstrumentUI.renderSelectors();
    };

    monInstrumentUI.toggleCategory = function(catId) {

        if (categoryOptions[catId] === 'visible') {
            hideCategory(catId);
        } else {
            showCategory(catId);
        }
    };

    monInstrumentUI.renderSelectors = function(callback) {

        // Add any missing categories to categoryOptions
        categoryOptions = persistentOptions.get('categoryOptions');
        for (var idx = 0; idx < monInstrumentData.instrumentCategories.length; idx++) {
            var catRec = monInstrumentData.instrumentCategories[idx];
            if (!categoryOptions[catRec.catId]) {
                categoryOptions[catRec.catId] = 'visible';
                persistentOptions.put('categoryOptions', categoryOptions);
            }
        }

        // Add hidden totals to visible total so they can be removed when hideCategory is run.
        visibleTotal = 0;
        for (var idx = 0; idx < monInstrumentData.instrumentCategories.length; idx++) {
            var catRec = monInstrumentData.instrumentCategories[idx];
            if (categoryOptions[catRec.catId] === 'hidden') {
                visibleTotal += catRec.categoryTotal; // This is backwards, but is fixed below.
            }
        }

        // Render category selectors containing instruments.
        $('#vscmmCategoryControls').empty(); 
        for (var idx = 0; idx < monInstrumentData.instrumentCategories.length; idx++) {

            var catRec = monInstrumentData.instrumentCategories[idx];
            if (catRec.categoryTotal === 0) { continue; }
            $('#vscmmCategoryControls').append(
                '<button class="btn btn-default vscmm-btn-category btn-primary" ' + 
                        'onclick="monInstrumentUI.toggleCategory(' + catRec.catId + ')" ' +
                        'data-category-id="' + catRec.catId + '">' +
                    '<div class="vscmm-left">' + 
                    '<img alt="Instrument Icon" src="' + catRec.iconUrl + '"/> ' + 
                    catRec.category + '</div> <span class="badge">' + catRec.categoryTotal + '</span>' + 
                '</button>'
            );

            if (categoryOptions[catRec.catId] === 'visible') {
                showCategory(catRec.catId);
            } else {
                hideCategory(catRec.catId);
            }
        }
        if ($('#vscmmCategoryControls').html() === '') { refreshUI(); } 
        if (typeof callback === 'function' && callback()) { callback(); }
    };

    // Below is for instrument popups. /////////////////////////////////////////////////////////////////////////////////
    
    monInstrumentUI.reShowInstModal = function() {
        
        monInstrumentUI.showInstModal(modalInstRec);
    };

    monInstrumentUI.showInstModal = function(instRec) {

        $('#vscmmEmptyModal .modal-title').html('');
        $('#vscmmEmptyModal .modal-body').html('');
        
        modalInstRec = instRec;

        $('#vscmmEmptyModalTitle').html(
                '<img alt="Instrument Icon" src="' + instRec.catRec.iconUrl + '"/> ' + 
                instRec.catRec.category + ' Monitoring Station'
        );
        
        var operatorHtml = '';
        if (instRec.operator && instRec.operator !== '') {
            operatorHtml = 'Operator: ' + instRec.operator + '<br/>';
        }
        
        var html = '<b>Station:</b> ' + instRec.station + '<br/>' + operatorHtml + '<br/>';
        
        for (var idx = 0; idx < instRec.output.length; idx++) {
            var imageRec = instRec.output[idx];
            html += 
                '<div class="vscmm-image-container">' +
                '<a href="javascript:monInstrumentUI.showImage(' + imageRec.outputId + ');" >' +
                    imageRec.caption + '<br/>' +
                    '<img src="' + imageRec.url + '" ' + 
                        'alt="' + imageRec.caption + '" ' + 
                        'title="' + imageRec.caption + '" width="125px" />' +
                '</a></div>';
        
                if ((idx + 1) % 4 === 0) { html += '<br/>'; }
        }

        if (modalInstRec.html.trim().length > 0) { html += modalInstRec.html; }

        $('#vscmmEmptyModalBody').html(html + '<br/>');
        $('#vscmmEmptyModal').removeClass('modal-wide');
        $('#vscmmEmptyModal').modal('show');
    };

    monInstrumentUI.showImage = function(outputId) {

        $('#vscmmEmptyModal .modal-title').html('');
        $('#vscmmEmptyModal .modal-body').html('');
       
        var imageRec = false, modalInstRec = false;
        for (var idx = 0; idx < monInstrumentData.instrumentRecs.length; idx++) {
            for (var idx2 = 0; idx2 < monInstrumentData.instrumentRecs[idx].output.length; idx2++) {
                if (parseInt(monInstrumentData.instrumentRecs[idx].output[idx2].outputId, 10) === 
                        parseInt(outputId, 10)) {
                    imageRec = monInstrumentData.instrumentRecs[idx].output[idx2];
                    modalInstRec = monInstrumentData.instrumentRecs[idx];
                    break;
                }
            }
        }

        var titleHtml = 
            '<img alt"Instrument Icon" src="' + modalInstRec.catRec.iconUrl + '"/> ' + 
            modalInstRec.catRec.category + ' Monitoring Station ' + modalInstRec.station;

        $('#vscmmEmptyModal .modal-title').html(titleHtml);

        var operatorHtml = '';
        if (modalInstRec.operator && modalInstRec.operator !== '') {
            operatorHtml = 'Operator: ' + modalInstRec.operator + '<br/>';
        }

        var bodyHtml = '<h4>' + imageRec.caption + '</h4>' + 
                (modalInstRec.instrument_html ? modalInstRec.instrument_html : '') + '<br/>' + operatorHtml + 
                '<img alt="Instrument data." src="' + imageRec.url + '" class="img-responsive"/>';

        bodyHtml += '<br/><a href="javascript:monInstrumentUI.reShowInstModal();" class="vscmm-left">' + 
                            'Back to monitoring station.</a>';

        if (imageRec.url && imageRec.url !== '') {
            bodyHtml += '<a href="' + imageRec.url + '" target="mon2Tab" ' + 
                    'class="vscmm-right">Open Image</a>';
        }

        $('#vscmmEmptyModal .modal-body').html(bodyHtml + '<br/>');
        
        $('#vscmmEmptyModal').addClass('modal-wide');        
        $('#vscmmEmptyModal').modal('show');
    };

    monInstrumentUI.showLess = function(evt) {

        $('#vscmmEmptyModal').modal('hide');
        $('#vscmmMapHover').hide();
        monQuakeMap.deactivateQuake();
        
        var topOffset = $('#vscmmMapContainer').position().top + evt.screenPoint.y + 12, //top_offset;
            leftOffset = $('#vscmmMapContainer').position().left + evt.screenPoint.x + 12; // Left offset

        if (!monMain.isDesktop) {
            topOffset = $('#vscmmMapContainerMobile').position().top + evt.screenPoint.y + 2, //top_offset;
            leftOffset = $('#vscmmMapContainerMobile').position().left + evt.screenPoint.x + 2; // Left offset
        } 

        modalInstRec = evt.graphic.attributes.instRec;

        $('#vscmmMapMobilePopup').css({ 'top': topOffset + 'px', 'left': leftOffset + 'px' });
        $('#vscmmMapMobilePopup').html(evt.graphic.attributes.popupHtml);
        $('#vscmmMapMobilePopup').show();
        
        if (!monMain.isDesktop) {
            if ((leftOffset + $('#vscmmMapMobilePopup').outerWidth()) > document.documentElement.clientWidth) {
                
                var newLeft = leftOffset - 153;
                if (newLeft < 0) { newLeft = 10; }
                
                $('#vscmmMapMobilePopup').css({
                    'left': newLeft + 'px',
                    'min-width': '153px'
                });
            }
        }
    };

}(window.monInstrumentUI = window.monInstrumentUI || {}, jQuery));
