// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
/* global esri */

(function (monMap, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var isInitialized = false, basemapGallery = {};
        
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    monMap.map = {};
    monMap.minLat = 0.0;
    monMap.maxLat = 0.0;
    monMap.minLong = 0.0;
    monMap.maxLong = 0.0;
    
    function buildMap(callback) { // Build map once scripts are loaded.

        $('body').addClass('claro');

        require(
            ['esri/map', 
                'esri/dijit/Scalebar', 'esri/dijit/OverviewMap', 'esri/geometry/webMercatorUtils', 
                'esri/dijit/BasemapGallery', 'esri/arcgis/utils', 'dojo/parser', 
                'dijit/layout/BorderContainer', 'dijit/layout/ContentPane', 'dijit/TitlePane', 'dojo/domReady!'], 
            function(Map, Scalebar, OverviewMap, webMercatorUtils, BasemapGallery, arcgisUtils, parser) {

            parser.parse();

            monMap.map = new Map((monMain.isDesktop ? 'vscmmMap' : 'vscmmMapMobile'), {
                basemap:         monUiOptions.basemap,
                showAttribution: false,
                logo:            false,
                center:          [monUiOptions.centerLong, monUiOptions.centerLat],
                zoom:            monUiOptions.mapZoom,
                smartNavigation: false
            });

            $('#vscmmMapInfo').html(
                '<a href="?centerLat=' + monUiOptions.centerLat + '&centerLong=' + monUiOptions.centerLong + 
                    '&mapZoom=' + monUiOptions.mapZoom + '">Link</a>'
            );

            // Add the overview map
            if (monMain.isDesktop) {
            
                var overviewMap = new OverviewMap({
                        map:          monMap.map,
                        visible:      true,
                        expandFactor: 10,
                        height:       128,
                        width:        128,
                        attachTo:     'bottom-right',
                        color:        'blue'
                    });
                overviewMap.startup();
            }
            
            monMap.map.on('load', function () {

                var geo = monMap.map.geographicExtent;
                monMap.minLat = geo.ymin;
                monMap.maxLat = geo.ymax;
                monMap.minLong = geo.xmin;
                monMap.maxLong = geo.xmax;

                // Add a scalebar
                Scalebar({ map : monMap.map, scalebarUnit : 'dual' });

                // Add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
                basemapGallery = new BasemapGallery({
                    showArcGISBasemaps: true,
                    map: monMap.map
                }, (monMain.isDesktop ? 'vscmmBasemapGallery': 'vscmmBasemapGalleryMobile'));
                basemapGallery.startup();

                basemapGallery.on('load', function(){
                    if (persistentOptions.get('esriBasemapId') !== '') { 
                        basemapGallery.select(persistentOptions.get('esriBasemapId'));
                    }
                });

                basemapGallery.on('selection-change',function(){
                    persistentOptions.put('esriBasemapId', basemapGallery.getSelected().id);
                });
    
                isInitialized = true;

                // DONE
                if (typeof callback === 'function' && callback()) { callback(); }
            });

            monMap.map.on('extent-change', function () {

                var centerPoint = monMap.map.extent.getCenter();
                centerPoint = webMercatorUtils.webMercatorToGeographic(centerPoint);  
                monUiOptions.centerLong = centerPoint.x;
                monUiOptions.centerLat = centerPoint.y;

                var geo = monMap.map.geographicExtent;
                monMap.minLat = geo.ymin;
                monMap.maxLat = geo.ymax;
                monMap.minLong = geo.xmin;
                monMap.maxLong = geo.xmax;
                monUiOptions.mapZoom = monMap.map.getZoom();

                // Extent changes are fired before map is intialized so we ignore them until true.
                if (isInitialized) { monMain.mapExtentChangeFiredOrFiltersChanged(); }
            });
        });
    }
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////
    
    monMap.isVisibleOnMap = function(lat, long) { // Returns true if provided point is within map extents.

        if (monMap.minLat === 0.0 && monMap.maxLat === 0.0 && monMap.minLong === 0.0 && monMap.maxLong === 0.0) {
            return true; // Unitialized, probably initial load.... don't apply any filters.
        }
        lat = parseFloat(lat);
        long = parseFloat(long);
        return (
            lat >= parseFloat(monMap.minLat) && lat <= parseFloat(monMap.maxLat) && 
            long >= parseFloat(monMap.minLong) && long <= parseFloat(monMap.maxLong)
        );
    };
    
    monMap.mapMouseOver = function (evt, callback) {
        
        $('#vscmmMapHover').html(evt.graphic.attributes.hoverText);
        
        var top = $('#vscmmMapContainer').position().top + evt.screenPoint.y + 12, //top_offset;
            left = $('#vscmmMapContainer').position().left + evt.screenPoint.x + 12; // Left offset

        if (!monMain.isDesktop) {
            top = $('#vscmmMapContainerMobile').position().top + evt.screenPoint.y + 12, //top_offset;
            left = $('#vscmmMapContainerMobile').position().left + evt.screenPoint.x + 12; // Left offset
        } 

        $('#vscmmMapHover').css({
            'border'           : 'solid 1px black', 
            'padding'          : '.25em',
            'background-color' : 'white', 
            'display'          : 'block', 
            'position'         : 'absolute', 
            'top'              : top + 'px',
            'left'             : left + 'px',
            'z-index'          : 999
        });
        
        if (typeof callback === 'function' && callback()) { callback(); }
    };
    
    monMap.mapMouseOut = function (callback) {
        
        $('#vscmmMapHover').hide();
        if (typeof callback === 'function' && callback()) { callback(); }
    };

    monMap.setCenter = function(longLat) {
        
        monUiOptions.centerLat = longLat[0];
        monUiOptions.centerLong = longLat[1];
        monMap.map.centerAt(longLat);
    };

    monMap.setZoom = function(mapZoom) {
        
        monUiOptions.mapZoom = mapZoom;
        monMap.map.setZoom(mapZoom);
    };

    monMap.setCenterAndZoom = function(longLat, mapZoom) {
        
        monUiOptions.centerLat = longLat[0];
        monUiOptions.centerLong = longLat[1];
        monUiOptions.mapZoom = mapZoom;
        monMap.map.centerAndZoom(longLat, mapZoom);
    };

    monMap.toClientLocation = function() {
        
        endUser.setPosition(function() {
            
            if (!monUtils.objectHasProperty(endUser.coords, 'latitude') || 
                    !monUtils.objectHasProperty(endUser.coords, 'longitude')) {
                return; // Coordinates are unavailable.
            }
            
            monUiOptions.centerLat = endUser.coords.latitude;
            monUiOptions.centerLong = endUser.coords.longitude;
            monMap.map.centerAt([monUiOptions.centerLong, monUiOptions.centerLat]);
        });
    };

    monMap.init = function (callback){ // Should only be called once.

        // Load scripts and css, once both control variables are true, build map.
        var cssLoaded = false, scriptsLoaded = false;

        monUtils.loadStylesheets(
            ['https://js.arcgis.com/3.23/dijit/themes/claro/claro.css', 'https://js.arcgis.com/3.23/esri/css/esri.css'], 
            function() {
                cssLoaded = true;
                if (scriptsLoaded) { buildMap(callback); }
            }
        );

        monUtils.loadScripts(
            ['https://js.arcgis.com/3.23/'], 
            function() {
                scriptsLoaded = true;
                if (cssLoaded) { buildMap(callback); }
            }
        );
    };
    
}(window.monMap = window.monMap || {}, jQuery));
