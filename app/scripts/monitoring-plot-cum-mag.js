// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
(function (monPlotCumMag, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var isInitialized = false, minTime = 0, maxTime = 0, dateTicks = 0, magTotal = 0.0, chartData = [];

    var chart = {
            'PADT': 5, 'PADR': 5, 'PADB': 20, 'PADL': 40, 'canvasH': 0, 'canvasW': 0, 'canvas': {}, 'context': {}
            };

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    function renderCumMag() {

        monQuakeMap.deactivateQuake();
        monQuakeList.tablerowInactive();
        
        chart.context.clearRect(0, 0, chart.canvasW, chart.canvasH); // Empty the canvas.
    
        var pxPerMs = (chart.canvasW - chart.PADL - chart.PADR) / (maxTime - minTime); // Pixel width / milliseconds
        
        monPlotChart.renderChartBox(chart); // Render box around grid top, right, bottom, left
        
        // Render x axis date labels and vertical date lines
        if (persistentOptions.get('utcOrLocal') === 'UTC') {
            
            for (
                var idx = (minTime + monUtils.getUTCOffsetMs()); 
                idx <= (maxTime + monUtils.getUTCOffsetMs()); 
                idx += (monUtils.MSDAY * dateTicks)
            ) {

                var pxX = ((idx - minTime) * pxPerMs) + chart.PADL;
                if (pxX < chart.PADL)  { continue; }
                monPlotChart.drawLine(chart.context, pxX, chart.PADT, pxX, chart.canvasH - chart.PADB); // Vertical line
                monPlotChart.drawText(chart.context, monUtils.dateYMD(new Date(idx), 'UTC'), pxX, 
                                        chart.canvasH - chart.PADB + 15, 'center');
            }
            
        } else if (persistentOptions.get('utcOrLocal') === 'local') {
            
            for (var idx = minTime; idx <= maxTime; idx += (monUtils.MSDAY * dateTicks)) {
                
                var pxX = ((idx - minTime) * pxPerMs) + chart.PADL;
                if (pxX < chart.PADL)  { continue; }
                monPlotChart.drawLine(chart.context, pxX, chart.PADT, pxX, chart.canvasH - chart.PADB); // Vertical line
                monPlotChart.drawText(chart.context, monUtils.dateYMD(new Date(idx), 'local'), pxX, 
                                        chart.canvasH - chart.PADB + 15, 'center');
            }
        }
       
        // CUMS
        var pxPerCum = (chart.canvasH - chart.PADT - chart.PADB) / magTotal;

        // Render y axis cum labels and horizontal lines
        var cumIncr = 5;
        if (magTotal > 1000) { cumIncr = 200; }
        else if (magTotal > 500) { cumIncr = 100; }
        else if (magTotal > 100) { cumIncr = 20; }
        else if (magTotal < 5) { cumIncr = 1; }
        
        for (var idx = 0; idx <= magTotal + 1; idx += cumIncr) {

            var pxY = monPlotChart.getPixelY(idx * pxPerCum, chart);
            monPlotChart.drawLine(chart.context, chart.PADL, pxY, chart.canvasW - chart.PADR, pxY); // Horiz cum lines.
            monPlotChart.drawText(chart.context, idx, chart.PADL - 4, pxY + 6, 'right');
        }

        // Render cum totals.
        monPlotChart.needsQuakeLabel = false;
        monPlotChart.color = 'blue';
        for(var idx = 0; idx < chartData.length; idx++) {
            
            var pxX1 = ((chartData[idx].unixTime - minTime) * pxPerMs) + chart.PADL;
            var pxY1 = monPlotChart.getPixelY(chartData[idx].magTotal * pxPerCum, chart);

            var displayTime = chartData[idx].quakeDateString + ' local';
            if (persistentOptions.get('utcOrLocal') === 'UTC') { displayTime = chartData[idx].quakeUTCString + ' UTC'; }
            
            var label = {
                'text1': 'Cumulative Magnitude: ' + monUtils.round(chartData[idx].magTotal,1),
                'text2': 'Date: ' + displayTime,
                'text3': 'Magnitude: ' + chartData[idx].mag
            };
            
            monPlotChart.drawCircle(chart.context, 4, pxX1, pxY1, label);

            if (idx > 0) {
                var pxX2 = ((lastDataRec.unixTime - minTime) * pxPerMs) + chart.PADL;
                var pxY2 = monPlotChart.getPixelY(lastDataRec.magTotal * pxPerCum, chart);
                monPlotChart.drawLine(chart.context, pxX2, pxY2, pxX1, pxY1);
            }
            var lastDataRec = chartData[idx];
        }
        monPlotChart.color = 'black';
        if (monPlotChart.needsQuakeLabel) { monPlotChart.drawLabel(chart.context); }
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monPlotCumMag.renderCumMag = function() {
        
        if (!isInitialized) {

            isInitialized = true;

            chart.canvas = document.getElementById('cumMagPlotCanvas');
            chart.context = chart.canvas.getContext('2d');
            chart.context.translate(0.5, 0.5); // Fixes issues with fuzzy lines.
            chart.canvasW = chart.canvas.width;
            chart.canvasH = chart.canvas.height;

            // Track canvas mouse position.
            chart.canvas.addEventListener('mousemove', function (evt) {
                var rect = chart.canvas.getBoundingClientRect();
                monPlotChart.mousePosition = {
                    x: evt.clientX - rect.left,
                    y: evt.clientY - rect.top
                };
                renderCumMag();
            }, false);
        }
        chart.context.clearRect(0, 0, chart.canvasW, chart.canvasH); // Empty the canvas.
        
        minTime = 0;
        maxTime = 0;
        magTotal = 0.0;
        
        chartData = [];
        for(var idx = 0; idx < monPlotChart.quakesInPolygon.length; idx++) {
            
            var quakeRec = monPlotChart.quakesInPolygon[idx];
            if (quakeRec.isFiltered) { continue; }
            
            chartData.push({
                    'unixTime'        : quakeRec.date.getTime(), 
                    'magTotal'        : 0,
                    'quakeDateString' : quakeRec.quakeDateString,
                    'quakeUTCString'  : quakeRec.quakeUTCString,
                    'mag'             : quakeRec.mag
                });
                
            magTotal += quakeRec.mag;
            
             // Time (use unix time value, e.g. 1489887407527 ms from 1970.)
            if (maxTime === 0) { maxTime = quakeRec.date.getTime(); }
            if (minTime === 0) {
                minTime = quakeRec.date.getTime();
            }
            if (maxTime < quakeRec.date.getTime()) { maxTime = quakeRec.date.getTime(); }
            if (minTime > quakeRec.date.getTime()) {
                minTime = quakeRec.date.getTime();
            }
        }
        magTotal++; // Add one more for the chart's y axis.
        
        // Put values in correct order, unixTime asc.
        chartData.sort(function(a,b) {return (a.unixTime > b.unixTime) ? 1 : ((b.unixTime > a.unixTime) ? -1 : 0);} );
       
        var runningMagTotal = 0.0;
        for (var idx = 0; idx < chartData.length; idx++) {
            runningMagTotal += chartData[idx].mag;
            chartData[idx].magTotal = runningMagTotal;
        }
        
        // Remove time components (h,m,s) from max/min date values.
        minTime = monUtils.removeTime(new Date(minTime)).getTime();
        maxTime = monUtils.removeTime(monUtils.addDays(new Date(maxTime), 1)).getTime();
        
        dateTicks = monUtils.dateDiffDD(new Date(maxTime), new Date(minTime));
        if (dateTicks > 5) { dateTicks = 5; }
        renderCumMag();
    };
    
}(window.monPlotCumMag = window.monPlotCumMag || {}, jQuery));
