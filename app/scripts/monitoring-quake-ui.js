// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com

(function (monQuakeUI, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var unfilteredTotal = 0, visibleOnMapTotal = 0,
        ageSelection = '',
        magToggle = {},
        depthToggle = {'0-5' : false, '5-10' : false, '10-15' : false, '15-20' : false, '20+' : false};

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function unHide() {
        persistentOptions.put('isHideAllQuakes', false);
    }
    
    function applyFilters() {
     
        unfilteredTotal = 0;
        visibleOnMapTotal = 0;

        for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) { // Set filter of hide all is selected.
            monQuakeData.quakeRecs[idx].isFiltered = persistentOptions.get('isHideAllQuakes');
            if (persistentOptions.get('isHideAllQuakes') && monQuakeData.quakeRecs[idx].isVisibleOnMap) {
                visibleOnMapTotal++;
            }
        }

        if (!persistentOptions.get('isHideAllQuakes')) {

            visibleOnMapTotal = 0;
            
            for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) {

                var quakeRec = monQuakeData.quakeRecs[idx];
                
                if (!quakeRec.isVisibleOnMap) { continue; }
                visibleOnMapTotal++;
                
                if (!quakeRec.isFiltered) {
                    var ageSelected = false;
                    if (ageSelection === 'last20' && quakeRec.isLast20) { ageSelected = true; }
                    else if (ageSelection === 'day' && quakeRec.ageDays <= 1) { ageSelected = true; }
                    else if (ageSelection === 'week' && quakeRec.ageDays <= 7) { ageSelected = true; }
                    else if (ageSelection === 'any') { ageSelected = true; }
                    quakeRec.isFiltered = !ageSelected;
                }  

                if (!quakeRec.isFiltered) {
                    var magSelected = false;
                    for (var key in magToggle) {
                        if (magToggle[key] && key === '0' && quakeRec.mag < 1) {
                            magSelected = true; 
                        }
                        if (magToggle[key] && key === '1' && quakeRec.mag >= 1 && quakeRec.mag < 2) { 
                            magSelected = true;
                        }
                        if (magToggle[key] && key === '2' && quakeRec.mag >= 2 && quakeRec.mag < 3) {
                            magSelected = true;
                        }
                        if (magToggle[key] && key === '3' && quakeRec.mag >= 3 && quakeRec.mag < 4) {
                            magSelected = true;
                        }
                        if (magToggle[key] && key === '4' && quakeRec.mag >= 4 && quakeRec.mag < 5) { 
                            magSelected = true; 
                        }
                        if (magToggle[key] && key === '5' && quakeRec.mag >= 5 && quakeRec.mag < 6) { 
                            magSelected = true; 
                        }
                        if (magToggle[key] && key === '6' && quakeRec.mag >= 6) { 
                            magSelected = true; 
                        }
                    }
                    quakeRec.isFiltered = !magSelected;
                }

                if (!quakeRec.isFiltered) {
                    var depthSelected = false;
                    for (var key in depthToggle) {
                        if (depthToggle[key] && key === '0-5' && quakeRec.depthKM < 5) { 
                            depthSelected = true; 
                        }
                        if (depthToggle[key] && key === '5-10' && quakeRec.depthKM >= 5 && quakeRec.depthKM < 10) { 
                            depthSelected = true; 
                        }
                        if (depthToggle[key] && key === '10-15' && quakeRec.depthKM >= 10 && quakeRec.depthKM < 15) { 
                            depthSelected = true; 
                        }
                        if (depthToggle[key] && key === '15-20' && quakeRec.depthKM >= 15 && quakeRec.depthKM < 20) { 
                            depthSelected = true; 
                        }
                        if (depthToggle[key] && key === '20+' && quakeRec.depthKM >= 20) { 
                            depthSelected = true; 
                        }
                    }
                    quakeRec.isFiltered = !depthSelected;
                }

                unfilteredTotal += (quakeRec.isFiltered ? 0 : 1);
            }
        }

        var visibleHtml = unfilteredTotal + ' Earthquakes Visible (of ' + visibleOnMapTotal + ')';
        if (unfilteredTotal === 1) {
            visibleHtml = unfilteredTotal + ' Earthquake Visible (of ' + visibleOnMapTotal + ')';
        }
        if (monUiOptions.mapZoom < monUiOptions.zoomThreshold) {
            visibleHtml += '<br/><br/><i>Zoom in to view lower magnitude quakes.</i>';
        }
            
        $('#vscmmQuakeTable').find('a').html(visibleHtml);

        if (monMain.isDesktop) {
            if (unfilteredTotal < visibleOnMapTotal) { $('.vscmm-btn-quake-show').fadeIn(); }
            if (unfilteredTotal === visibleOnMapTotal) { $('.vscmm-btn-quake-show').fadeOut(); }
            if (unfilteredTotal > 0) { $('.vscmm-btn-quake-hide').fadeIn(); }
            if (unfilteredTotal === 0) { $('.vscmm-btn-quake-hide').fadeOut(); }
        } else {
            if (unfilteredTotal < visibleOnMapTotal) { $('.vscmm-btn-quake-show').show(); }
            if (unfilteredTotal === visibleOnMapTotal) { $('.vscmm-btn-quake-show').hide(); }
            if (unfilteredTotal > 0) { $('.vscmm-btn-quake-hide').show(); }
            if (unfilteredTotal === 0) { $('.vscmm-btn-quake-hide').hide(); }
        }
    };

    function hideAllUI() {

        persistentOptions.put('isHideAllQuakes', true);

        ageSelection = '';
        persistentOptions.put('quakeAgeSelection', '');
        $('.btn-age').removeClass('btn-primary');
        $('.btn-age').addClass('btn-default');
        
        for (var idx = 0; idx < 7; idx++) { magToggle[idx] = false; }
        persistentOptions.put('quakeMagToggle', magToggle);
        $('.btn-mag').removeClass('btn-primary');
        $('.btn-mag').addClass('btn-default');

        for (var key in depthToggle) { depthToggle[key] = false; }
        persistentOptions.put('quakeDepthToggle', depthToggle);
        $('.btn-depth').removeClass('btn-primary');
        $('.btn-depth').addClass('btn-default');
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monQuakeUI.showLess = function(evt) {

        $('#vscmmEmptyModal').modal('hide');
        $('#vscmmMapHover').hide();

        var topOffset = $('#vscmmMapContainer').position().top + evt.screenPoint.y + 12, //top_offset;
            leftOffset = $('#vscmmMapContainer').position().left + evt.screenPoint.x + 12; // Left offset

        if (!monMain.isDesktop) {
            topOffset = $('#vscmmMapContainerMobile').position().top + evt.screenPoint.y + 2, //top_offset;
            leftOffset = $('#vscmmMapContainerMobile').position().left + evt.screenPoint.x + 2; // Left offset
        } 

        monQuakeMap.activateQuake(evt.graphic.attributes.eventId);
        $('#vscmmMapMobilePopup').css({ 'top': topOffset + 'px', 'left': leftOffset + 'px' });
        $('#vscmmMapMobilePopup').html(evt.graphic.attributes.popupHtml);
        $('#vscmmMapMobilePopup').show();

        if (!monMain.isDesktop) {
            if ((leftOffset + $('#vscmmMapMobilePopup').outerWidth()) > document.documentElement.clientWidth) {
                
                var newLeft = leftOffset - 214;
                if (newLeft < 0) { newLeft = 10; }
                
                $('#vscmmMapMobilePopup').css({
                    'left': newLeft + 'px',
                    'min-width': '214px'
                });
            }
        }
    };

    monQuakeUI.showDetail = function(eventId) {

        $('#vscmmEmptyModal .modal-title').html('Earthquake Report');
        $('#vscmmEmptyModal .modal-body').html('Retrieving data...');

        monQuakeData.getDetail(eventId, function(detailRec) {

            var phaseData = detailRec.properties.products['phase-data'][0].properties;

            var col1Html =

                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Coordinates:</b></div> ' +
                '    <div class="col col-md-7">' + 
                            detailRec.lat + ', ' + detailRec.long + 
                '    </div> ' +
                '</div> ' +
                
                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Depth:</b></div> ' +
                '    <div class="col col-md-7">' + 
                        detailRec.depthKM + ' km ' + 
                        '(' + monUtils.round(monUtils.kmToMi(detailRec.depthKM), 1) + ' mi)' + 
                '    </div> ' +
                '</div> ' +
                
                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Magnitude:</b></div> ' +
                '    <div class="col col-md-7">' + detailRec.properties.mag + '</div> ' +
                '</div> ' +

                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Date:</b></div> ' +
                '    <div class="col col-md-7">' +
                        detailRec.quakeDateString + ' local<br/>' + 
                        detailRec.quakeUTCString + ' UTC<br/>' + 
                        'Age: ' + monUtils.round(detailRec.ageDays, 1) + ' days' + 
                '    </div> ' +
                '</div> ' +

                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Distance From:</b></div> ' +
                '    <div class="col col-md-7">' + detailRec.properties.place + '</div> ' +
                '</div> ' +
                
                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Number of Phases:</b></div> ' +
                '    <div class="col col-md-7">' + phaseData['num-phases-used'] + '</div> ' +
                '</div> ' +
                
                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Azimuthal Gap (deg):</b></div> ' +
                '    <div class="col col-md-7">' + phaseData['azimuthal-gap'] + '</div> ' +
                '</div> ' +

                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Distance to Nearest Station:</b></div> ' +
                '    <div class="col col-md-7">' + 
                        monUtils.round(phaseData['minimum-distance'], 2)  + ' deg' +
                '    </div> ' +
                '</div> ' +
                
                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Vertical / Horizontal Error:</b></div> ' +
                '    <div class="col col-md-7">' + 
                        phaseData['vertical-error'] + ' km / ' + phaseData['horizontal-error'] + ' km' +
                '    </div> ' +
                '</div> ' +
                                
                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Event ID:</b></div> ' +
                '    <div class="col col-md-7">' + eventId + '</div> ' +
                '</div> ' +
                
                '<div class="row vscmm-hover"> ' +
                '    <div class="col col-md-5"><b>Contributing Network:</b></div> ' +
                '    <div class="col col-md-7">' + 
                        detailRec.properties.products.origin['0'].properties['origin-source'] + '</div>' + 
                '</div> ' +
                
                '';
        
                var col2Html =
                    
                    '<div class="row vscmm-hover"> ' +
                    '    <div class="col col-md-12"><b>Earthquake Hazard Program</b></div> ' +
                    '</div> ' +
                    
                    '<div class="row vscmm-hover"> ' +
                    '    <div class="col col-md-12">' + 
                            '<a href="' + detailRec.properties.url + '" target="ehptab">' + 
                                'Additional information: ' + detailRec.properties.title + '</a>' + 
                    '    </div> ' +
                    '</div> ' +
                    
                    '<div class="row vscmm-hover"> ' +
                    '    <div class="col col-md-12">' + 
                            '<a href="https://earthquake.usgs.gov/earthquakes/eventpage/' + eventId + '#tellus" ' + 
                                'target="ehptab">Did You Feel It?</a>' +
                    '    </div> ' +
                    '</div> ' +
                    
                    '';
            
            var bodyHtml =
                    '<div class="row"> ' +
                    '    <div class="col col-md-8">' + col1Html + '</div>' +
                    '    <div class="col col-md-4">' + col2Html + '</div>' +
                    '</div> ';

            $('#vscmmEmptyModal .modal-body').html(bodyHtml);
        });
        $('#vscmmEmptyModal').removeClass('modal-wide');
        $('#vscmmEmptyModal').modal('show');
    };
    
    monQuakeUI.showAll = function() {
                
        monQuakeUI.reset();
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };
    
    monQuakeUI.hideAll = function() {

        hideAllUI();
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };
    
    monQuakeUI.setAge = function(age) {
        
        if (persistentOptions.get('isHideAllQuakes')) {
            unHide();
            for (var key in depthToggle) { monQuakeUI.toggleDepth(key); }
            for (var key in magToggle) { monQuakeUI.toggleMag(key); }
        }
        $('.btn-age').removeClass('btn-primary');
        $('.btn-age').addClass('btn-default');
        ageSelection = age;
        $('.btn-age[data-age="' + age + '"]').removeClass('btn-default');
        $('.btn-age[data-age="' + age + '"]').addClass('btn-primary');
        persistentOptions.put('quakeAgeSelection', age);
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };
    
    monQuakeUI.toggleMag = function(mag) {
        
        if (persistentOptions.get('isHideAllQuakes')) {
            unHide();
            if (ageSelection === '') { monQuakeUI.setAge('any'); }
            for (var key in depthToggle) { monQuakeUI.toggleDepth(key); }
        }

        if (magToggle[mag]) {
            magToggle[mag] = false;
            $('.btn-mag[data-mag="' + mag + '"]').removeClass('btn-primary');
            $('.btn-mag[data-mag="' + mag + '"]').addClass('btn-default');
        } else {
            magToggle[mag] = true;
            $('.btn-mag[data-mag="' + mag + '"]').removeClass('btn-default');
            $('.btn-mag[data-mag="' + mag + '"]').addClass('btn-primary');
        }
        persistentOptions.put('quakeMagToggle', magToggle);
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };

    monQuakeUI.toggleDepth = function(depthSelection) {
      
        if (persistentOptions.get('isHideAllQuakes')) {
            unHide();
            if (ageSelection === '') { monQuakeUI.setAge('any'); }
            for (var key in magToggle) { monQuakeUI.toggleMag(key); }
        }

        if (depthToggle[depthSelection]) {
            depthToggle[depthSelection] = false;
            $('.btn-depth[data-depth="' + depthSelection + '"]').removeClass('btn-primary');
            $('.btn-depth[data-depth="' + depthSelection + '"]').addClass('btn-default');
        } else {
            depthToggle[depthSelection] = true;
            $('.btn-depth[data-depth="' + depthSelection + '"]').removeClass('btn-default');
            $('.btn-depth[data-depth="' + depthSelection + '"]').addClass('btn-primary');
        }
        persistentOptions.put('quakeDepthToggle', depthToggle);
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };
    
    monQuakeUI.reset = function() {

        unHide();
        persistentOptions.put('quakeAgeSelection', 'any');
        persistentOptions.put('quakeMagToggle', false);
        persistentOptions.put('quakeDepthToggle', false);
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };
    
    monQuakeUI.resetOnly = function() {

        unHide();
        persistentOptions.put('quakeAgeSelection', 'any');
        persistentOptions.put('quakeMagToggle', false);
        persistentOptions.put('quakeDepthToggle', false);
    };

    monQuakeUI.refresh = function() {

        $('.btn-age').removeClass('btn-primary');
        $('.btn-age').addClass('btn-default');
        ageSelection = persistentOptions.get('quakeAgeSelection');

        $('.btn-age[data-age="' + ageSelection + '"]').removeClass('btn-default');
        $('.btn-age[data-age="' + ageSelection + '"]').addClass('btn-primary');
        
        if (persistentOptions.get('quakeMagToggle')) {
            magToggle = persistentOptions.get('quakeMagToggle');
        } else {
            for (var idx = 0; idx < 7; idx++) { magToggle[idx] = true; }
            persistentOptions.put('quakeMagToggle', magToggle);
        }
        for (var key in magToggle) {
            if (magToggle[key]) {
                $('.btn-mag[data-mag="' + key + '"]').removeClass('btn-default');
                $('.btn-mag[data-mag="' + key + '"]').addClass('btn-primary');
            }
        }

        if (persistentOptions.get('quakeDepthToggle')) {
            depthToggle = persistentOptions.get('quakeDepthToggle');
        } else {
            for (var key in depthToggle) { depthToggle[key] = true; }
            persistentOptions.put('quakeDepthToggle', depthToggle);
        }
        for (var key in depthToggle) {
            if (depthToggle[key]) {
                $('.btn-depth[data-depth="' + key + '"]').removeClass('btn-default');
                $('.btn-depth[data-depth="' + key + '"]').addClass('btn-primary');
            }
        }
        applyFilters();

        for (var key in monQuakeData.magBuckets) {
            if (monQuakeData.magBuckets[key] > 0) {
                $('.btn-mag[data-mag="' + key + '"]').slideDown();
            } else {
                $('.btn-mag[data-mag="' + key + '"]').slideUp();
            }
            $('.btn-mag[data-mag="' + key + '"] > .badge').html(monQuakeData.magBuckets[key]);
        }

        for (var key in monQuakeData.depthBuckets) {
            if (monQuakeData.depthBuckets[key] > 0) {
                $('.btn-depth[data-depth="' + key + '"]').slideDown();
            } else {
                $('.btn-depth[data-depth="' + key + '"]').slideUp();
            }
            $('.btn-depth[data-depth="' + key + '"] > .badge').html(monQuakeData.depthBuckets[key]);
        }

        for (var key in monQuakeData.ageBuckets) {
            if (monQuakeData.ageBuckets[key] > 0) {
                $('.btn-age[data-age="' + key + '"]').slideDown();
            } else {
                $('.btn-age[data-age="' + key + '"]').slideUp();
            }
            $('.btn-age[data-age="' + key + '"] > .badge').html(monQuakeData.ageBuckets[key]);
        }
        $('#vscmmLast20Label').html('Last ' + monQuakeData.ageBuckets['last20']);
        
        if (persistentOptions.get('isHideAllQuakes')) {
            hideAllUI();
            applyFilters();
        }
    };

}(window.monQuakeUI = window.monQuakeUI || {}, jQuery));
