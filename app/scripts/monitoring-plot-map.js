// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
/* global dojo, esri */
(function (monPlotMap, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var isSelectingPoints = false, recPoly = {}, polygonPoints = [];
    
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    monPlotMap.mapLayer = {};
    monPlotMap.p1 = {};
    monPlotMap.p2 = {};
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function addPointToLayer(point) {

        var symbol = new esri.symbol.SimpleMarkerSymbol(
            esri.symbol.SimpleMarkerSymbol.STYLE_CROSS,
            20,
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_CROSS, new dojo.Color('red'), 1),
            new dojo.Color('red')
        );
        var graphic = new esri.Graphic(point, symbol);
        monPlotMap.mapLayer.add(graphic);

        var textSymbol =  new esri.symbol.TextSymbol((point.pointnum === 1 ? 'A' : 'B'))
                                .setColor(new dojo.Color('red'))
                                .setAlign(esri.symbol.Font.ALIGN_START)
                                .setOffset(8, 8)
                                .setVerticalAlignment('middle')
                                .setFont(
                                    new esri.symbol.Font('12pt')
                                    .setFamily('Arial')
                                    .setWeight(esri.symbol.Font.WEIGHT_BOLD)
                                );
                    
        var textGraphic = new esri.Graphic(point, textSymbol);
        monPlotMap.mapLayer.add(textGraphic);
    }

    // Lifted from VOLCWEB's xsec.js, then modified a bit. Rectangle logic by Weston Thelen and Scott Pincus.
    function renderPolygonAndSelectQuakes(callback) { 

        // Reset the layer, this may be called from several different functions.
        if (!monPlotMap.p1.x || !monPlotMap.p2.x) { return; }
        monPlotMap.mapLayer.clear();
        addPointToLayer(monPlotMap.p1);
        addPointToLayer(monPlotMap.p2);
        recPoly = {};
        polygonPoints = [];
        
        //
        var fullWidth = $('#vscmmPlotWidth').val();
        var halfWidth = (parseFloat(fullWidth) ? fullWidth / 2 : 5); // W is 1/2 width in KM, defaults to 5.

        // Widths are too small - until we come up with an exact solution, I'm just adding to halfWidth
        halfWidth = halfWidth * 1.35;

        // Get angle of cross section line
        var phirad = Math.atan(((monPlotMap.p2.y - monPlotMap.p1.y) / (monPlotMap.p2.x - monPlotMap.p1.x))); // radians
        var phideg = Math.abs((180 / Math.PI) * phirad);		//convert from radians to degrees for debugging

        // Adjust based on which sector was clicked first------------------------------------------
        // x coordinate increases to the right, and the y coordinate increases downwards
        var LLUR = 0, ULLR = 0, LRUL = 0, URLL = 0, locTemp = {};
        if (monPlotMap.p1.x < monPlotMap.p2.x && monPlotMap.p1.y > monPlotMap.p2.y) { ULLR = 1; } // Upper L to lower R
        if (monPlotMap.p1.x < monPlotMap.p2.x && monPlotMap.p1.y < monPlotMap.p2.y) { LLUR = 1; } // Lower L to upper R
        if (monPlotMap.p1.x > monPlotMap.p2.x && monPlotMap.p1.y > monPlotMap.p2.y ) { // Upper right to lower left
            URLL = 1;
            locTemp = monPlotMap.p2; //swap points so it acts like ULLR
            monPlotMap.p2 = monPlotMap.p1;
            monPlotMap.p1 = locTemp;
        }
        if (monPlotMap.p1.x > monPlotMap.p2.x && monPlotMap.p1.y < monPlotMap.p2.y) { // Lower right to upper left
            LRUL = 1;
            locTemp = monPlotMap.p2; //swap points so it acts like LLUR
            monPlotMap.p2 = monPlotMap.p1;
            monPlotMap.p1 = locTemp;
        }	

        // Calculate distance ratio in map/spatial coords 
        // monPlotMap.p1 is in map/spatial coords of a user clicked event. monPlotMap.p1 set in calling function
        var p1 = new esri.geometry.Point(monPlotMap.p1);

        // offset y of our local point (monPlotMap.p1) in map cord by 1 spatial unit
        var p2 = new esri.geometry.Point(monPlotMap.p1);
        p2.setY(p2.y + 1);
        // get distance of the offset in km
        var ydist = esri.geometry.getLength(p1, p2) / 1000;

        // offset x of our local point in map cord by 1 spatial unit
        var p3 = new esri.geometry.Point(monPlotMap.p1);
        p3.setX(p3.x + 1);
        // get distance of the offset in km
        var xdist = esri.geometry.getLength(p1, p3) / 1000;

        // Find latitude and longitude offsets (in pixels)-----------------------------------------
        // Absolute value allows me to be even sloppier with my math
        var dely = Math.abs(halfWidth / ydist * Math.sin((Math.PI / 180) * (90 - phideg)));	//Convert to radians dummy
        var delx = Math.abs(halfWidth / xdist * Math.cos((Math.PI / 180) * (90 - phideg)));

        // Get rectangle vertex points---------------------------------------------------------------------------
        // In pixels (despite name)
        //Point one is furthest south, and subsequent points are ccw
        if (ULLR === 1 || LRUL === 1) {
            var vert1 = new esri.geometry.Point(monPlotMap.p2);
            vert1.setX(vert1.x - delx);
            vert1.setY(vert1.y - dely);
            var vert2 = new esri.geometry.Point(monPlotMap.p2);
            vert2.setX(vert2.x + delx);
            vert2.setY(vert2.y + dely);
            var vert3 = new esri.geometry.Point(monPlotMap.p1);
            vert3.setX(vert3.x + delx);
            vert3.setY(vert3.y + dely);
            var vert4 = new esri.geometry.Point(monPlotMap.p1);
            vert4.setX(vert4.x - delx);
            vert4.setY(vert4.y - dely);
            if (LRUL === 1){		// swap points back from above
                locTemp = monPlotMap.p2;
                monPlotMap.p2 = monPlotMap.p1;
                monPlotMap.p1 = locTemp;
            }
        }

        if (LLUR === 1 || URLL === 1) {
            var vert1 = new esri.geometry.Point(monPlotMap.p2);
            vert1.setX(vert1.x + delx);
            vert1.setY(vert1.y - dely);
            var vert2 = new esri.geometry.Point(monPlotMap.p1);
            vert2.setX(vert2.x + delx);
            vert2.setY(vert2.y - dely);
            var vert3 = new esri.geometry.Point(monPlotMap.p1);
            vert3.setX(vert3.x - delx);
            vert3.setY(vert3.y + dely);
            var vert4 = new esri.geometry.Point(monPlotMap.p2);
            vert4.setX(vert4.x - delx);
            vert4.setY(vert4.y + dely);
            if (URLL === 1){		// swap points back from above
                locTemp = monPlotMap.p2;
                monPlotMap.p2 = monPlotMap.p1;
                monPlotMap.p1 = locTemp;
            }
        }

        polygonPoints = [vert3, vert2, vert1, vert4, vert3];

        recPoly = new esri.geometry.Polygon(monMap.map.spatialReference);
        recPoly.addRing(polygonPoints);

        var recOutline = new esri.symbol.SimpleLineSymbol();
        recOutline.setColor (new dojo.Color('black'), 1);
        recOutline.setWidth(.25);

        var recSymbol = new esri.symbol.SimpleFillSymbol(
                esri.symbol.SimpleFillSymbol.STYLE_SOLID, 
                recOutline, 
                new dojo.Color([255, 255, 0, 0.25])
            );
    
        monPlotMap.mapLayer.add(new esri.Graphic(recPoly, recSymbol));

        var pointA = (monPlotMap.p1.pointnum === 1 ? monPlotMap.p1 : monPlotMap.p2);
        // Set data for quakes inside the polygon.
        monPlotChart.quakesInPolygon = [];
        for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) {
            var quakeRec = monQuakeData.quakeRecs[idx];
            if (quakeRec.isFiltered) { continue; }
            var point = new esri.geometry.Point(quakeRec.long, quakeRec.lat);
    		if (recPoly.contains(point)) {
                var d = monUtils.distanceM(pointA.getLatitude(), pointA.getLongitude(), quakeRec.lat, quakeRec.long);
                quakeRec.distFromStartKM = monUtils.round(d / 1000, 3);
                monPlotChart.quakesInPolygon.push(quakeRec);
            }
        }

        $('.plot-ready').show();
        monPlotChart.renderAll();
        
        if (typeof callback === 'function' && callback()) { callback(); }
    }	

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monPlotMap.enableCapture = function() {
        
        $('.plot-active').show();
        $('.plot-inactive').hide();
        $('.plot-ready').hide();
        isSelectingPoints = true;
        monPlotMap.mapLayer.clear();
        monPlotMap.p1 = {};
        monPlotMap.p2 = {};
    };

    monPlotMap.redraw = function() {

        renderPolygonAndSelectQuakes();
    };
    
    monPlotMap.incrementWidth = function(incVal) {
        
        var widthVal = parseFloat($('#vscmmPlotWidth').val());
        if (!widthVal || widthVal + incVal < 0) {
            widthVal = 1;
        } else {
            widthVal = widthVal + incVal;
        }
        $('#vscmmPlotWidth').val(widthVal);
        monPlotMap.redraw();
    };
    
    monPlotMap.reset = function() {
        
        $('.plot-active').hide();
        $('.plot-inactive').show();
        $('.plot-ready').hide();
        isSelectingPoints = false;
        monPlotMap.mapLayer.clear();
        monPlotMap.p1 = {};
        monPlotMap.p2 = {};
        recPoly = {},
        polygonPoints = [],
        monPlotChart.quakesInPolygon = [];
    };

    monPlotMap.init = function (){
                
        // Set vscmmPlotWidth input value based on map zoom
        if (monUiOptions.mapZoom > 12) {
            $('#vscmmPlotWidth').val(1);
        } if (monUiOptions.mapZoom === 12) {
            $('#vscmmPlotWidth').val(5);
        } if (monUiOptions.mapZoom < 12) {
            $('#vscmmPlotWidth').val(10);
        }
        
        $('.plot-active').hide();
        $('.plot-inactive').show();
        $('.plot-ready').hide();
        
        monPlotMap.mapLayer = new esri.layers.GraphicsLayer();
        monMap.map.addLayer(monPlotMap.mapLayer);
        monMap.map.on('click', function(event) {
            if (!isSelectingPoints) { return; }
            var clickPoint = event.mapPoint;
            if (!monPlotMap.p1.x) {
                monPlotMap.p1 = clickPoint;
                monPlotMap.p1.pointnum = 1;
                addPointToLayer(clickPoint);
            } else if (!monPlotMap.p2.y) {
                monPlotMap.p2 = clickPoint;
                monPlotMap.p2.pointnum = 2;
                addPointToLayer(clickPoint);
                renderPolygonAndSelectQuakes();
                //isSelectingPoints = false;
            } else {
                monPlotMap.mapLayer.clear();
                monPlotMap.p1 = clickPoint;
                monPlotMap.p1.pointnum = 1;
                addPointToLayer(clickPoint);
                monPlotMap.p2 = {};
            }
        });
    };
    
}(window.monPlotMap = window.monPlotMap || {}, jQuery));
