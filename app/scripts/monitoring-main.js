// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
/* global esri, monUiOptions */
(function (monMain, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var MOBILE_MAX_WIDTH = 991, areQuakesInitialized = false, areInstrumentsInitialized = false, 
        mobileGenerated = false, mobileLaidOut = false, desktopGenerated = false, shouldRefreshBucketTotals = false;
    
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////
    
    monMain.isDesktop           = true;

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    
    function layoutDesktop() {

        $('#vscmmDesktopContainer').show();
        $('#vscmmMobileContainer').hide();
       
        var dcHeight = document.documentElement.clientHeight * .90;
        $('#vscmmDesktopContainer').height(dcHeight);

        $('.vscmm-quake-table-container').css('max-height', dcHeight / 2);
        //
        $('#vscmmMapContainer').width(
            $('#vscmmDesktopContainer').outerWidth() - 
                $('#vscmmDesktopContainer > .container-controls').outerWidth() - 4
        );

        if($('#vscmmMapHover').length === 0) { // Add element to display map mouse over information
            $('#vscmmMapContainer').after('<div id="vscmmMapHover"></div>');
            $('#vscmmMapHover').hide();
        }

        if($('#vscmmMapInfo').length === 0) { // Add element to display link for different locations.
            $('#vscmmDesktopContainer > .container-controls').append('<div id="vscmmMapInfo"></div>');
        }
        
        $('#vscmmDesktopContainer > .container-controls').css({'position': 'relative', 'top': '-30px'});
        $('.vscmm-map-tools').css('vscmm-right', $('.vscmm-map-tools').width());
        
        // Generate and position legend over map.
        $('#vscmmLegend')
                .css({
                    'position': 'absolute',
                    'top': $('#vscmmMapContainer').position().top + 
                                $('#vscmmMapContainer').height() - $('#vscmmLegend').height() - 240,
                    'left': $('#vscmmMapContainer').position().left + 20
                    })
                .draggable();

        // Center #vscmmLegendShowButton
        $('#vscmmLegendShowButton').css({
                'position': 'absolute',
                'top': $('#vscmmMapContainer').position().top + 
                            $('#vscmmMapContainer').height() - $('#vscmmLegendShowButton').height() - 5,
                'left': $('#vscmmMapContainer').position().left + 
                            ($('#vscmmMapContainer').width() / 2) - ($('#vscmmLegendShowButton').width() / 2)
            });

        if (persistentOptions.get('legendSetting') === 'app') {
            monUI.toggleLegend('on');
        } else {
            monUI.toggleLegend(persistentOptions.get('legendSetting'));
        }

        monUI.toggleAutoData(persistentOptions.get('automaticData'));
        
        // Position loading information box
        $('#vscmmLoading')
            .css({
                'position': 'absolute',
                'top': $('#vscmmMapContainer').position().top + 20,
                'left': $('#vscmmMapContainer').position().left + 60
                })
            .draggable();
    }
    
    function layoutMobile() {

        if (mobileLaidOut) { return; } // Window resize is firing when not desired on quake list - prevents breaking ui.

        $('#vscmmDesktopContainer').hide();
        $('#vscmmMobileContainer').show();

        $('#vscmmMapContainerMobile').height(document.documentElement.clientHeight * .95);
        $('#vscmmQuakeListMobileContainer').hide();
        $('.quake-list-visible').hide();

        //
        $('#vscmmMobileContainer > .container-controls').width('30px');

        $('#vscmmMapContainerMobile,#vscmmQuakeListMobileContainer').width(
            $('#vscmmMobileContainer').outerWidth() - $('#vscmmMobileContainer > .container-controls').outerWidth() - 2
        );

        // Generate and position legend over map.
        $('#vscmmLegend')
                .css({
                    'position': 'absolute',
                    'top': $('#vscmmMapContainerMobile').position().top + 
                                $('#vscmmMapContainerMobile').height() - $('#vscmmLegend').height() - 240,
                    'left': $('#vscmmMapContainerMobile').position().left + 20
                    })
                .draggable();

        // Center #vscmmLegendShowButton
        $('#vscmmLegendShowButton').css({
                'position': 'absolute',
                'top': $('#vscmmMapContainerMobile').position().top + 
                                $('#vscmmMapContainerMobile').height() - $('#vscmmLegendShowButton').height() - 5,
                'left': $('#vscmmMapContainerMobile').position().left + 
                            ($('#vscmmMapContainerMobile').width() / 2) - ($('#vscmmLegendShowButton').width() / 2)
            });
            
        if (persistentOptions.get('legendSetting') === 'app') {
            monUI.toggleLegend('off');
        } else {
            monUI.toggleLegend(persistentOptions.get('legendSetting'));
        }
        
        // Position loading information box
        $('#vscmmLoading')
            .css({
                'position': 'absolute',
                'top': $('#vscmmMapContainerMobile').position().top + 20,
                'left': $('#vscmmMapContainerMobile').position().left + 60
                })
            .draggable();
        
        mobileLaidOut = true;
    }
    
    function generateDesktopContent() {
        
        desktopGenerated = true;
        layoutDesktop();
        initMap();
    }
    
    function generateMobileContent() {

        mobileGenerated = true;
        layoutMobile();
        
        //
        if($('#vscmmMapMobilePopup').length === 0) { // Add element to display map click information on mobile only.
            $('#vscmmMapContainerMobile').after('<div id="vscmmMapMobilePopup"></div>');
            $('#vscmmMapMobilePopup').hide();
        }
        $('#vscmmMapMobilePopup').css({
            'border'           : 'solid 1px black', 
            'padding'          : '.25em',
            'background-color' : 'white', 
            'display'          : 'block', 
            'position'         : 'absolute', 
            'z-index'          : 999
        });

        if($('#vscmmMapHover').length === 0) { // Add element to display map mouse over information
            $('#vscmmMapContainerMobile').after('<div id="vscmmMapHover"></div>');
            $('#vscmmMapHover').hide();
        }

        if($('#vscmmMapInfo').length === 0) { // Add element to display link for different locations.
            $('#vscmmMobileContainer > .container-controls').append('<div id="vscmmMapInfo"></div>');
        }

        initMap();
    }
    
    function initMap() {
            
        monMap.init(function(){
            
            if (monUiOptions.showBoundariesOnMap) { monMapBoundaries.addBoundaries(); }
            
            monPlotMap.init();
            monVolcMap.addMapLayers();
            monQuakeMap.addMapLayers();
            
            monMain.mapExtentChangeFiredOrFiltersChanged();

            // If eventid parameter is provided, look up the event and center the map on it, also display event popup.
            if (monUiOptions.eventId && monUiOptions.eventId.trim().length > 0) {

                monQuakeData.getDetail(monUiOptions.eventId, function(data) {

                    // Add function to quake data queue, to run after data pull is complete.
                    var displayFunc =  function() { monQuakeUI.showDetail(monUiOptions.eventId); };
                    monQuakeData.functionQueue.push(displayFunc);

                    // Trigger data pull
                    monMap.setCenterAndZoom(
                            [data.geometry.coordinates[0], data.geometry.coordinates[1]], monUiOptions.mapZoom
                        );
                 });
            }
        });
    }

    function refreshQuakeUIcontent(shouldRefreshBucketTotalsIn) {
        
        shouldRefreshBucketTotals = shouldRefreshBucketTotalsIn;

        $('#vscmmLoading').show(400, function () {

            setTimeout(function() { // Gives vscmmLoading box time to display.
                $('#vscmmLoading .loading-message').html('Processing earthquake data...');
                if (shouldRefreshBucketTotals) { monQuakeData.refreshBucketTotals(); }
                monQuakeUI.refresh();
                monQuakeMap.renderQuakes();
                if (monMain.isDesktop) {
                    monQuakeList.renderHtmlTable();
                } else {
                    monQuakeList.renderRowsAndCols();
                }
                monPlotChart.renderAll();
                monLegend.renderQuakeInfo();
                areQuakesInitialized = true;
                $('#vscmmLoading').fadeOut();
            }, 100);
        });
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monMain.mapExtentChangeFiredOrFiltersChanged = function () {

        $('#vscmmMapMobilePopup').hide();

        $('#vscmmMapInfo').html(
            '<!--<a href="?centerLat=' + monUiOptions.centerLat + '&centerLong=' + monUiOptions.centerLong + 
                '&mapZoom=' + monUiOptions.mapZoom + '">Link</a>-->'
        );

        if (!monUiOptions.showInstruments && !areQuakesInitialized) { monQuakeUI.resetOnly(); }

        if (monQuakeData.isDataAlreadyPulled(monMap.minLong, monMap.minLat, monMap.maxLong, monMap.maxLat)) {
            refreshQuakeUIcontent(true);
            return;
        }

        monQuakeData.getQuakes(monMap.minLong, monMap.minLat, monMap.maxLong, monMap.maxLat, function() {
            refreshQuakeUIcontent(false);
        });

        monInstrumentData.getInstrumentsIfNeeded(monMap.minLong, monMap.minLat, monMap.maxLong, monMap.maxLat, 
            function() {

                var params = '?lat1=' + monMap.minLat + '&long1=' + monMap.minLong + '&lat2=' + monMap.maxLat + 
                        '&long2=' + monMap.maxLong;
                /*
                console.log(
                    monUiOptions.instrumentsUrl + params,
                    monInstrumentData.instrumentRecs,
                    monInstrumentData.instrumentCategories,
                    monInstrumentData.instrumentTypes,
                    monInstrumentData.hasApproximateLocations
                );
                */
                if (!areInstrumentsInitialized) {
                    areInstrumentsInitialized = true;
                    if (!monUiOptions.showInstruments) { monInstrumentUI.showHideAll(false); }
                }
                monInstMap.refreshInstruments();
                monInstrumentUI.renderSelectors();
                monLegend.renderInstCats();
            }
        );
        
        monVolcData.getRegion(monMap.minLong, monMap.minLat, monMap.maxLong, monMap.maxLat, function() {
            monVolcMap.addOrRefreshVolcanoes(function() {
                monVolcMap.showHideVolcNamesBasedOnZoom(monUiOptions.mapZoom);
            });
            monLegend.renderVolcanoes();
        });
    };

    monMain.init = function (){

        monUiOptions.init();
        
        monUI.setUIForDepthUnits();
        monUI.setUIForTimeUnits();
        monUI.setUIForQuakeColor();

        $('#monitoringMapContainer')
            .empty().load(monUiOptions.monitoringPath + 'templates/monitoring-map.html', function() {
            
            $('#vscmmMonitoringTitle').html(monUiOptions.title);

            if ($('body').width() > MOBILE_MAX_WIDTH) {
                monMain.isDesktop = true;
                generateDesktopContent();
            } else {
                monMain.isDesktop = false;
                generateMobileContent();
            }
        });
    };
    
    monMain.showSettingsModal = function() {

        $('#vscmmSettingsModal').modal('show');
        monUI.setUIForDepthUnits();
        monUI.setUIForTimeUnits();
        monUI.setUIForQuakeColor();
    };
    
    monMain.showHelpModal = function() {

        $('#vscmmHelpModal').modal('show');
    };
   
    $(window).resize(function() {

        if ($('body').width() > MOBILE_MAX_WIDTH) {
            monMain.isDesktop = true;
            if (desktopGenerated) {
                layoutDesktop();
            } else {
                window.location.reload();
            }
        } else {
            monMain.isDesktop = false;
            if (mobileGenerated) {
                layoutMobile();
            } else {
                window.location.reload();
            }
        }
    });
 
}(window.monMain = window.monMain || {}, jQuery));

$(document).ready(function (){
    
    monMain.init();
});