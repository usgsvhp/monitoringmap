// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
(function (monPlotChart, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////
    var isInitialized = false;
    
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////
    monPlotChart.quakeLabel = {};
    monPlotChart.needsQuakeLabel = false; // Quake label when mouse over needs to be rendered AFTER all quakes.
    monPlotChart.quakesInPolygon = [];
    monPlotChart.mousePosition = {};
    monPlotChart.boldText = false;
    monPlotChart.color = 'black';
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////
    monPlotChart.renderChartBox = function(chart) {
        
        monPlotChart.drawLine(chart.context, chart.PADL, chart.PADT, chart.canvasW - chart.PADR, chart.PADT);
        monPlotChart.drawLine(chart.context, chart.canvasW - chart.PADR, chart.PADT, chart.canvasW - chart.PADR, 
                                chart.canvasH - chart.PADB);
        monPlotChart.drawLine(chart.context, chart.PADL, chart.canvasH - chart.PADB, chart.canvasW - chart.PADR, 
                                chart.canvasH - chart.PADB);
        monPlotChart.drawLine(chart.context, chart.PADL, chart.PADT, chart.PADL, chart.canvasH - chart.PADB);
    };
    
    // Return the pixel Y position within the data area of the chart. Inverts the Y axis within the actual chart region.
    monPlotChart.getPixelY = function(y, chart) {
        
        y = Math.round(y);
        var gridH = chart.canvasH - chart.PADT - chart.PADB;
        return gridH - y + chart.PADT;
    };
            
    monPlotChart.drawCircle = function(context, r, x, y, label) {
       
        r = Math.round(r);
        x = Math.round(x);
        y = Math.round(y);
        
        context.beginPath();
        context.arc(x, y, r, 0, 2 * Math.PI, false);
        context.fillStyle = monPlotChart.color;
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = 'gray';
        context.stroke();
        
        // Is mouse inside circle? if radius from xy to mouse pointer is less than circle radius - YES.
        // If in circle, render text.
        var mouseDistance = Math.sqrt(
                    Math.pow((x - monPlotChart.mousePosition.x), 2) + Math.pow((y - monPlotChart.mousePosition.y), 2)
                    );
            
        if (mouseDistance < r) { // Mouse is inside quake circle.

            monPlotChart.needsQuakeLabel = true;
            
            var rectX = x + r + 4, rectY = y - 4;
            if ((rectX + 180) > context.canvas.width) { // Render text to left of quake instead of default right.
                rectX = rectX - 180 - r - r - 8;
            }
            if (((rectY + 44) > context.canvas.height)) { // Render text above bottom of canvas.
                rectY = rectY - 44;
            }

            monPlotChart.quakeLabel = label;
            monPlotChart.quakeLabel = {
                'rectX'   : rectX,
                'rectY'   : rectY,
                'text1'   : label.text1,
                'text2'   : label.text2,
                'text3'   : label.text3,
                'eventId' : 0
            };
        }
    };
            
    monPlotChart.drawLabel = function(context) {
        
        context.beginPath();
        context.rect(monPlotChart.quakeLabel.rectX, monPlotChart.quakeLabel.rectY, 180, 44);
        context.fillStyle = 'white';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = monPlotChart.color;
        context.stroke();
        // Draw text
        monPlotChart.drawText(context, monPlotChart.quakeLabel.text1, monPlotChart.quakeLabel.rectX + 2, 
                                monPlotChart.quakeLabel.rectY + 14, 'left', monPlotChart.color);
        monPlotChart.drawText(context, monPlotChart.quakeLabel.text2, monPlotChart.quakeLabel.rectX + 2, 
                                monPlotChart.quakeLabel.rectY + 26, 'left', monPlotChart.color);
        monPlotChart.drawText(context, monPlotChart.quakeLabel.text3, monPlotChart.quakeLabel.rectX + 2, 
                                monPlotChart.quakeLabel.rectY + 38, 'left', monPlotChart.color);
    };
            
            
    monPlotChart.drawQuakeCircle = function(context, quakeRec, x, y) {
        
        x = Math.round(x);
        y = Math.round(y);
        
        var color = monUtils.getDepthColor(quakeRec.depthKM),
            radius = monUtils.getQuakePixelDiameter(quakeRec.mag) / 2;

        if (persistentOptions.get('quakeColorChoice') === 'time') { color = monUtils.getTimeColor(quakeRec.ageDays); }

        context.beginPath();
        context.arc(x, y, radius, 0, 2 * Math.PI, false);
        context.fillStyle = color;
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = 'gray';
        context.stroke();
        
        // Is mouse inside circle? if radius from xy to mouse pointer is less than circle radius - YES.
        // If in circle, render text.
        var mouseDistance = Math.sqrt(
                    Math.pow((x - monPlotChart.mousePosition.x), 2) + Math.pow((y - monPlotChart.mousePosition.y), 2)
                    );
        if (mouseDistance < radius) { // Mouse is inside quake circle.
            
            monPlotChart.needsQuakeLabel = true;
            
            // Rectangle coordinates for text background.
            var rectX = x + radius + 4, rectY = y - 4;
            if ((rectX + 180) > context.canvas.width) { // Render text to left of quake instead of default right.
                rectX = rectX - 180 - radius - radius - 8;
            }
            if (((rectY + 44) > context.canvas.height)) { // Render text above bottom of canvas.
                rectY = rectY - 44;
            }

            // Only one label is displayed so it will be for top quake if stacked.
            var displayTime = quakeRec.quakeDateString + ' local';
            if (persistentOptions.get('utcOrLocal') === 'UTC') { displayTime = quakeRec.quakeUTCString + ' UTC'; }

            var displayDepth = monUtils.round(monUtils.kmToMi(quakeRec.depthKM), 1) + ' mi';
            if (persistentOptions.get('kmOrMi') === 'km') { displayDepth = quakeRec.depthKM + ' km'; }

            monPlotChart.quakeLabel = {
                'rectX'   : rectX,
                'rectY'   : rectY,
                'text1'   : 'Magnitude: ' + quakeRec.mag,
                'text2'   : 'Depth: ' + displayDepth,
                'text3'   : 'Date: ' + displayTime,
                'eventId' : quakeRec.eventId
            };
        }
    };
   
    monPlotChart.drawQuakeLabel = function(context) {

        context.beginPath();
        context.rect(monPlotChart.quakeLabel.rectX, monPlotChart.quakeLabel.rectY, 180, 44);
        context.fillStyle = 'white';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = monPlotChart.color;
        context.stroke();
        // Draw text
        monPlotChart.drawText(context, monPlotChart.quakeLabel.text1, monPlotChart.quakeLabel.rectX + 2, 
                                monPlotChart.quakeLabel.rectY + 14, 'left', monPlotChart.color);
        monPlotChart.drawText(context, monPlotChart.quakeLabel.text2, monPlotChart.quakeLabel.rectX + 2, 
                                monPlotChart.quakeLabel.rectY + 26, 'left', monPlotChart.color);
        monPlotChart.drawText(context, monPlotChart.quakeLabel.text3, monPlotChart.quakeLabel.rectX + 2, 
                                monPlotChart.quakeLabel.rectY + 38, 'left', monPlotChart.color);
        
        monQuakeMap.activateQuake(monPlotChart.quakeLabel.eventId);
        monQuakeList.tablerowActive(monPlotChart.quakeLabel.eventId);
    };
    
    monPlotChart.drawText = function(context, text, x, y, textAlign) {
        
        x = Math.round(x);
        y = Math.round(y);
        context.beginPath();
        context.fillStyle = monPlotChart.color;
        context.strokeStyle = monPlotChart.color;
        if (monPlotChart.boldText) {
            context.font = 'bold 16px sans-serif';
        } else {
            context.font = 'normal 12px sans-serif';    
        }
        context.textAlign = textAlign;
        context.fillText(text, x, y);
        context.stroke();
    };

    monPlotChart.drawLine = function(context, x1, y1, x2, y2) {

        x1 = Math.round(x1);
        y1 = Math.round(y1);
        x2 = Math.round(x2);
        y2 = Math.round(y2);
        context.beginPath();
        context.lineWidth = 1;
        context.strokeStyle = monPlotChart.color;
        context.moveTo(x1, y1); // top left
        context.lineTo(x2, y2);
        context.stroke();
    };
    
    monPlotChart.renderAll = function() {
        
        if (!isInitialized) {
            isInitialized = true;
            $('#vscmmPlotModal').draggable();
        }
        monPlotTimeDepth.renderTimeDepth();
        monPlotCumMag.renderCumMag();
        monPlotCrossSection.renderCrossSection();
        monPlotCumTotal.renderCumTotal();
    };
    
}(window.monPlotChart = window.monPlotChart || {}, jQuery));
