// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
/* global esri, dojo */
(function (monInstMap, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var LONG_PAD = .0006, categoryLayer = false, instPointsUsed = []; // Used to keep track of instruments that overlap.

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function getImageWidthHeight(iconUrl) {
        
        var iconFileName = iconUrl.substring(iconUrl.lastIndexOf('/') + 1);
        
        switch(iconFileName) {
            case 'gage.png':
                return [17, 17];
                break;
            case 'gas.png':
                return [18, 12];
                break;
            case 'gps.png':
                return [17, 17];
                break;
            case 'icon_no.png':
                return [20, 20];
                break;
            case 'lahar.png':
                return [15, 15];
                break;
            case 'pressure.png':
                return [15, 15];
                break;
            case 'seismometer.png':
                return [15, 13];
                break;
            case 'temperature.png':
                return [9, 15];
                break;
            case 'tiltmeter.png':
                return [16, 10];
                break;
            case 'webcam.png':
                return [15, 13];
                break;
            default:
                return [15, 15];
        }
    }

    function isInstPointUsed(lat, long) {

        for (var idx = 0; idx < instPointsUsed.length; idx++) {
            if (instPointsUsed[idx].lat === lat && instPointsUsed[idx].long === long) { return true; }
        }
        instPointsUsed.push({ 'lat': lat, 'long' : long });
        return false;
    }

    function makeInstrumentGraphic(instrumentRec) {
   
        var thisLat = parseFloat(instrumentRec.lat), thisLong = parseFloat(instrumentRec.long);
        // Add a bit of long to overlapping instrument markers if needed so they line up instead of overlapping.
        while(isInstPointUsed(thisLat, thisLong)) { thisLong = thisLong + LONG_PAD; }
                
        var point = new esri.geometry.Point(thisLong, thisLat);
        point = esri.geometry.geographicToWebMercator(point);

        var wh = getImageWidthHeight(instrumentRec.catRec.iconUrl);

        var symbol = new esri.symbol.PictureMarkerSymbol(
                instrumentRec.catRec.iconUrl, wh[0], wh[1] // image url, width, height
            );

        var imgHtml = '';
        for (var idx = 0; idx < instrumentRec.output.length; idx++) {
            var imageRec = instrumentRec.output[idx];
            imgHtml += 
                '<div class="vscmm-image-container">' +
                '<a href="javascript:monInstrumentUI.showImage(' + imageRec.outputId + ');" >' +
                    imageRec.caption + '<br/>' +
                    '<img src="' + imageRec.url + '" alt="' + imageRec.caption + '" ' + 
                        'title="' + imageRec.caption + '" width="44px" height="60px" />' +
                '</a></div>';
        }

        var attribs = {
                'hoverText' : instrumentRec.catRec.category + ' - ' + instrumentRec.station + 
                                    '<br/><b><i>Click for additional information.</i></b>',
                'popupHtml' : instrumentRec.catRec.category + ' - ' + instrumentRec.station + 
                                    '<br/>' + 
                                    '<a href="javascript:monInstrumentUI.reShowInstModal();">' + 
                                        'Instrument Information</a><br/>' + 
                                    '<a href="javascript:$(\'#vscmmMapMobilePopup\').hide();">CLOSE</a>',
                'instRec'   : instrumentRec
            };
        return new esri.Graphic(point, symbol, attribs);
    }
    
    function mapMouseOver(evt) {

        monMap.mapMouseOver(evt, function() {
            $('#vscmmMapHover').html('');
            $('#vscmmMapHover').html(evt.graphic.attributes.hoverText);
        });
    }
    
    function itemClicked(evt) {

        monMap.mapMouseOut();
        if (monMain.isDesktop) {
            monInstrumentUI.showInstModal(evt.graphic.attributes.instRec);
        } else {
            monInstrumentUI.showLess(evt);
        }
    }
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////
 
    monInstMap.refreshInstruments = function(callback) { // One layer for each category, run when extent changes.
        
        $('div.titleButton.close').click(); // Close graphic if open.
                
        if (!categoryLayer) {
            categoryLayer = new esri.layers.GraphicsLayer();
            monMap.map.addLayer(categoryLayer);
            if (monMain.isDesktop) {
                dojo.connect(categoryLayer, 'onMouseOver', mapMouseOver);
                dojo.connect(categoryLayer, 'onMouseOut', monMap.mapMouseOut);
            }
            dojo.connect(categoryLayer, 'onClick', itemClicked);
        }
        categoryLayer.clear();
        instPointsUsed = [];
        
        // Add  list of instruments to map category layer.
        for (var idx = 0; idx < monInstrumentData.instrumentRecs.length; idx++) {
            if (!monInstrumentUI.isCategoryVisible(monInstrumentData.instrumentRecs[idx].catId)) { continue; }
            categoryLayer.add(makeInstrumentGraphic(monInstrumentData.instrumentRecs[idx]));
        }
        
        if (typeof callback === 'function' && callback()) { callback(); }
    };

}(window.monInstMap = window.monInstMap || {}, jQuery));
