// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
(function (monPlotCrossSection, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var isInitialized = false, DIST_TICKS = 10,
        maxMag = -99.0, minMag = 99.0, maxDepth = -99.0, minDepth = 99.0, maxDistFromStartKM = 99; // boundary values
        
    var chart = {
                'PADT': 5, 'PADR': 20, 'PADB': 30, 'PADL': 40, 'canvasH': 0, 'canvasW': 0, 'canvas': {}, 'context': {}
                };
        
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    function renderCrossSection() {

        monQuakeMap.deactivateQuake();
        monQuakeList.tablerowInactive();
        
        chart.context.clearRect(0, 0, chart.canvasW, chart.canvasH); // Empty the canvas.
    
        var xTickWidthPx = (chart.canvasW - chart.PADL - chart.PADR) / DIST_TICKS,
            pxXPerKm = (chart.canvasW - chart.PADL - chart.PADR) / maxDistFromStartKM,
            kmPerPxX = maxDistFromStartKM / (chart.canvasW - chart.PADL - chart.PADR),
            pxPerDepthKM = 0.0;

        monPlotChart.renderChartBox(chart); // Render box around grid top, right, bottom, left
        
        // Render x axis labels and vertical lines
        for (var idx = 0; idx <= DIST_TICKS + 1; idx ++) {

            var pxX = (idx * xTickWidthPx) + chart.PADL;
            monPlotChart.drawLine(chart.context, pxX, chart.PADT, pxX, chart.canvasH - chart.PADB); // Vertical line
            
            if (idx === 0) {
                monPlotChart.boldText = true;
                monPlotChart.color = 'red';
                monPlotChart.drawText(chart.context, 'A', pxX, chart.canvasH - chart.PADB + 28, 'center');
                monPlotChart.color = 'black';
                monPlotChart.boldText = false;
            }
            
            var km = (pxX - chart.PADL) * kmPerPxX;
            var distString = monUtils.round(monUtils.kmToMi(km), 1) + ' mi';
            if (persistentOptions.get('kmOrMi') === 'km') { distString = monUtils.round((km), 1) + ' km'; }
            monPlotChart.drawText(chart.context, distString, pxX, chart.canvasH - chart.PADB + 15, 'center');
        }
        
        monPlotChart.boldText = true;
        monPlotChart.color = 'red';
        monPlotChart.drawText(
                            chart.context, 'B', chart.canvasW - chart.PADR, chart.canvasH - chart.PADB + 28, 'center'
                            );
        monPlotChart.color = 'black';
        monPlotChart.boldText = false;
                
        // DEPTHS
        // Because depths can be negative this gets complicated, negative depths are above sea level.
        var maxDepthAbs = 0.0, depthOffset = 0.0;
        if (maxDepth < 0) { // All depths above sea level
            maxDepthAbs = Math.ceil(Math.abs(maxDepth));
            depthOffset = Math.abs(Math.floor(minDepth));
        } else if (minDepth < 0 && maxDepth > 0) { // Mix of depths
            maxDepthAbs = Math.abs(Math.floor(minDepth)) + Math.ceil(maxDepth);
            depthOffset = Math.abs(Math.floor(minDepth));
        } else if (minDepth >= 0) { // All depths below sea level.
            maxDepthAbs = Math.ceil(maxDepth);
            depthOffset = 0.0;
        } else {
            // console.log('ERROR: none of the above, minDepth: ' + minDepth + ' - maxDepth: ' + maxDepth);
        }
        pxPerDepthKM = (chart.canvasH - chart.PADT - chart.PADB) / maxDepthAbs;

        // Render y axis depth labels and horizontal lines
        for (var idx = Math.floor(minDepth); idx <= Math.ceil(maxDepth); idx += 2) {
            
            var pxY = ((idx + depthOffset) * pxPerDepthKM) + chart.PADT;
            monPlotChart.drawLine(chart.context, chart.PADL, pxY, chart.canvasW - chart.PADR, pxY); // Horiz depth lines
            
            var depthString = monUtils.round(monUtils.kmToMi(idx), 1) + ' mi';
            if (persistentOptions.get('kmOrMi') === 'km') { depthString = idx + ' km'; }
            
            monPlotChart.drawText(chart.context, depthString, chart.PADL - 4, pxY + 6, 'right');
        }

        // Render quakes in plot.
        monPlotChart.needsQuakeLabel = false;
        for(var idx = 0; idx < monPlotChart.quakesInPolygon.length; idx++) {

            var quakeRec = monPlotChart.quakesInPolygon[idx];
            if (quakeRec.isFiltered) { continue; }
            var pxX = (parseFloat(quakeRec.distFromStartKM) * pxXPerKm) + chart.PADL;
            var pxY = ((parseFloat(quakeRec.depthKM) + depthOffset) * pxPerDepthKM) + chart.PADT;
            monPlotChart.drawQuakeCircle(chart.context, quakeRec, pxX, pxY);
        }
        if (monPlotChart.needsQuakeLabel) { monPlotChart.drawQuakeLabel(chart.context); }
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monPlotCrossSection.renderCrossSection = function() {
        
        if (!isInitialized) {

            isInitialized = true;

            chart.canvas = document.getElementById('crossSectionPlotCanvas');
            chart.context = chart.canvas.getContext('2d');
            chart.context.translate(0.5, 0.5); // Fixes issues with fuzzy lines.
            chart.canvasW = chart.canvas.width;
            chart.canvasH = chart.canvas.height;

            // Track canvas mouse position.
            chart.canvas.addEventListener('mousemove', function (evt) {
                var rect = chart.canvas.getBoundingClientRect();
                monPlotChart.mousePosition = {
                    x: evt.clientX - rect.left,
                    y: evt.clientY - rect.top
                };
                renderCrossSection();
            }, false);
        }
        chart.context.clearRect(0, 0, chart.canvasW, chart.canvasH); // Empty the canvas.
        
        // determine boundary values;
        maxMag = -999.0;
        minMag = 999.0;
        maxDepth = -999.0;
        minDepth = 999.0;
        maxDistFromStartKM = 0;
        
        var pointA = (monPlotMap.p1.pointnum === 1 ? monPlotMap.p1 : monPlotMap.p2);
        var pointB = (monPlotMap.p1.pointnum === 2 ? monPlotMap.p1 : monPlotMap.p2);
        if (typeof pointA.getLatitude === 'function' && typeof pointB.getLatitude === 'function') {
            var distM = monUtils.distanceM(
                                pointA.getLatitude(), pointA.getLongitude(), pointB.getLatitude(), pointB.getLongitude()
                                );
            maxDistFromStartKM = distM / 1000;
        }
        
        for(var idx = 0; idx < monPlotChart.quakesInPolygon.length; idx++) {
            
            var quakeRec = monPlotChart.quakesInPolygon[idx];
            if (quakeRec.isFiltered) { continue; }
            // Magnitude 
            if (maxMag < parseFloat(quakeRec.mag)) { maxMag = parseFloat(quakeRec.mag); }
            if (minMag > parseFloat(quakeRec.mag)) { minMag = parseFloat(quakeRec.mag); }
            // Depth KM
            if (maxDepth < parseFloat(quakeRec.depthKM)) { maxDepth = parseFloat(quakeRec.depthKM); }
            if (minDepth > parseFloat(quakeRec.depthKM)) { minDepth = parseFloat(quakeRec.depthKM); }
        }
        renderCrossSection();
    };
    
}(window.monPlotCrossSection = window.monPlotCrossSection || {}, jQuery));
