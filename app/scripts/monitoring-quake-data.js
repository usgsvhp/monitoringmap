// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com

(function (monQuakeData, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var urlParamsLastTry = '', detailRecs = {}, 
        dataMinLong = 0.0, dataMaxLong = 0.0, dataMinLat = 0.0, dataMaxLat = 0.0, thresholdStatus = '';

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    monQuakeData.quakeRecs = [];
    monQuakeData.minMag = 0.0;
    monQuakeData.maxMag = 0.0;
    monQuakeData.minDepthKM = 0.0;
    monQuakeData.maxDepthKM = 0.0;
    monQuakeData.ageBuckets = {};
    monQuakeData.magBuckets = {};
    monQuakeData.depthBuckets = {};

    // FUNCTION QUEUE:
    // https://stackoverflow.com/questions/899102/how-do-i-store-javascript-functions-in-a-queue-for-them-to-be-executed-eventuall
    monQuakeData.functionQueue = [];
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    
    function processDetailAfterPull(detailRec) {
        
        detailRec.lat = detailRec.geometry.coordinates[0];
        detailRec.long = detailRec.geometry.coordinates[1];
        detailRec.quakeDate = new Date(detailRec.properties.time);
        detailRec.quakeUTCString = monUtils.dateToUTCString(detailRec.quakeDate);
        detailRec.quakeDateString = monUtils.dateToString(detailRec.quakeDate);
        detailRec.ageDays = monUtils.dateDiffHH(new Date(), detailRec.quakeDate) / 24;
        detailRec.mag = Math.round(parseFloat(detailRec.properties.mag) * 10) / 10;
        detailRec.depthKM = detailRec.geometry.coordinates[2];
            
        return detailRec;
    }
    
    function refreshQuakeData() {
        
        monQuakeData.ageBuckets['day'] = 0;
        monQuakeData.ageBuckets['week'] = 0;
        monQuakeData.ageBuckets['any'] = 0;
        monQuakeData.ageBuckets['last20'] = 0;
        
        monQuakeData.magBuckets['0'] = 0;
        monQuakeData.magBuckets['1'] = 0;
        monQuakeData.magBuckets['2'] = 0;
        monQuakeData.magBuckets['3'] = 0;
        monQuakeData.magBuckets['4'] = 0;
        monQuakeData.magBuckets['5'] = 0;
        monQuakeData.magBuckets['6'] = 0;
        
        monQuakeData.depthBuckets['0-5'] = 0;
        monQuakeData.depthBuckets['5-10'] = 0;
        monQuakeData.depthBuckets['10-15'] = 0;
        monQuakeData.depthBuckets['15-20'] = 0;
        monQuakeData.depthBuckets['20+'] = 0;
                
        var countTo20 = 0;

        // Sort by unixDate desc.
        monQuakeData.quakeRecs.sort(sortQuakesNewest);

        for (var idx = 0, totalQuakes = monQuakeData.quakeRecs.length; idx < totalQuakes; idx++) {

            var quakeRec = monQuakeData.quakeRecs[idx];
            
            quakeRec.isVisibleOnMap = monMap.isVisibleOnMap(quakeRec.lat, quakeRec.long);
            quakeRec.isFiltered = false;
            quakeRec.isLast20 = (quakeRec.isVisibleOnMap ? countTo20 < 20 : false);
            if (quakeRec.isVisibleOnMap) { countTo20++; }
            if (!quakeRec.isVisibleOnMap) { continue; }

            switch(Math.floor(quakeRec.mag)) {
                case 0:
                    monQuakeData.magBuckets['0'] += 1;
                    break;
                case 1:
                    monQuakeData.magBuckets['1'] += 1;
                    break;
                case 2:
                    monQuakeData.magBuckets['2'] += 1;
                    break;
                case 3:
                    monQuakeData.magBuckets['3'] += 1;
                    break;
                case 4:
                    monQuakeData.magBuckets['4'] += 1;
                    break;
                case 5:
                    monQuakeData.magBuckets['5'] += 1;
                    break;
                default:
                    if (quakeRec.mag < 0) {
                        monQuakeData.magBuckets['0'] += 1;
                    } else {
                        monQuakeData.magBuckets['6'] += 1;
                    }
            }

            if (quakeRec.depthKM < 5) {
                monQuakeData.depthBuckets['0-5'] += 1;
            } else if (quakeRec.depthKM < 10) {
                monQuakeData.depthBuckets['5-10'] += 1;
            } else if (quakeRec.depthKM < 15) {
                monQuakeData.depthBuckets['10-15'] += 1;
            } else if (quakeRec.depthKM < 20) {
                monQuakeData.depthBuckets['15-20'] += 1;
            } else {
                monQuakeData.depthBuckets['20+'] += 1;
            }

            // Set up monQuakeData.ageBuckets
            if (quakeRec.isLast20) { monQuakeData.ageBuckets['last20']++; }
            if (quakeRec.ageDays < 1) { monQuakeData.ageBuckets['day']++; }
            if (quakeRec.ageDays < 7) { monQuakeData.ageBuckets['week']++; }
            monQuakeData.ageBuckets['any']++;

            if (quakeRec.mag < monQuakeData.minMag) { monQuakeData.minMag = quakeRec.mag; }
            if (quakeRec.mag > monQuakeData.maxMag) { monQuakeData.maxMag = quakeRec.mag; }
        
            if (quakeRec.depthKM < monQuakeData.minDepthKM) { monQuakeData.minDepthKM = quakeRec.depthKM; }
            if (quakeRec.depthKM > monQuakeData.maxDepthKM) { monQuakeData.maxDepthKM = quakeRec.depthKM; }
        }
    }
    
    function sortQuakesNewest(a, b) {
        
        if (parseFloat(a['unixTime']) > parseFloat(b['unixTime'])) { return -1; }
        if (parseFloat(a['unixTime']) < parseFloat(b['unixTime'])) { return 1; }
        return 0;
    }
    
    function getQuakeDataFromFDNWS(minLongIn, minLatIn, maxLongIn, maxLatIn, callback) {
        
        var now = new Date();
        
        var urlParams = 
            '?maxlatitude=' + maxLatIn + '&minlatitude=' + minLatIn + '&maxlongitude=' + maxLongIn + 
            '&minlongitude=' + minLongIn + '&orderby=time' + 
            (monUiOptions.mapZoom < monUiOptions.zoomThreshold ? '&minmagnitude=3' : '');
console.log('Earthquake API data URL: ', monUiOptions.quakesUrl + '.geojson' + urlParams);
        $.getJSON(monUiOptions.quakesUrl + '.geojson' + urlParams, function(data) {

            for (var idx = 0; idx < data.features.length; idx++) {
                
                var fdnwsRec = data.features[idx], 
                    lat = fdnwsRec.geometry.coordinates[1], 
                    long = fdnwsRec.geometry.coordinates[0];

                //if (lat > maxLatIn || lat < minLatIn || long > maxLongIn || long < minLongIn) { continue; }

                var quakeRec = {};
                quakeRec.eventId = fdnwsRec.id;
                quakeRec.date = new Date(fdnwsRec.properties.time);
                quakeRec.unixTime = fdnwsRec.properties.time;
                quakeRec.tzMnts = fdnwsRec.properties.tz;
                quakeRec.url = fdnwsRec.properties.url;
                quakeRec.isVisibleOnMap = monMap.isVisibleOnMap(quakeRec.lat, quakeRec.long);
                quakeRec.quakeUTCString = monUtils.dateToUTCString(quakeRec.date);
                quakeRec.quakeDateString = monUtils.dateToString(quakeRec.date);
                quakeRec.ageDays = monUtils.dateDiffHH(now, quakeRec.date) / 24;
                quakeRec.mag = Math.round(parseFloat(fdnwsRec.properties.mag) * 10) / 10;
                quakeRec.depthKM = Math.round(parseFloat(fdnwsRec.geometry.coordinates[2]) * 10) / 10;
                quakeRec.long = parseFloat(long);
                quakeRec.lat = parseFloat(lat);

                monQuakeData.quakeRecs.push(quakeRec);
            }

            if (typeof callback === 'function' && callback()) { callback(); }
        });
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monQuakeData.updateQuakeData = function() { // Refresh quake data without changing map extents.

        monUtils.preventAccordion = true;
        dataMinLong = 0.0;
        dataMaxLong = 0.0;
        dataMinLat = 0.0;
        dataMaxLat = 0.0;
        thresholdStatus = '';
        urlParamsLastTry = '';
        monQuakeData.quakeRecs = [];
        monQuakeData.minMag = 0.0;
        monQuakeData.maxMag = 0.0;
        monQuakeData.minDepthKM = 0.0;
        monQuakeData.maxDepthKM = 0.0;
        monQuakeData.ageBuckets = {};
        monQuakeData.magBuckets = {};
        monQuakeData.depthBuckets = {};
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };

    monQuakeData.refreshBucketTotals = function() {
        
        refreshQuakeData();
    };

    monQuakeData.isDataAlreadyPulled = function(minLongIn, minLatIn, maxLongIn, maxLatIn) {

        var currentThresholdStatus = (monUiOptions.mapZoom < monUiOptions.zoomThreshold ? 'over' : 'under');

        if (dataMinLong === 0.0 && dataMaxLong === 0.0 && dataMinLat === 0.0 && dataMaxLat === 0.0) {
            dataMinLong = minLongIn;
            dataMaxLong = maxLongIn;
            dataMinLat = minLatIn;
            dataMaxLat = maxLatIn;
            thresholdStatus = currentThresholdStatus;
            return false;
        }

        if (currentThresholdStatus !== thresholdStatus || 
                dataMinLong > minLongIn || dataMaxLong < maxLongIn || dataMinLat > minLatIn || dataMaxLat < maxLatIn) {
            
            thresholdStatus = currentThresholdStatus;
            dataMinLong = minLongIn;
            dataMaxLong = maxLongIn;
            dataMinLat = minLatIn;
            dataMaxLat = maxLatIn;
            return false;
        }

        return true;
    };
    
    monQuakeData.getQuakeRec = function(eventId) {

        for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) {
            if (monQuakeData.quakeRecs[idx].eventId === eventId) { return monQuakeData.quakeRecs[idx]; }
        }
        return {}; // Nothing found.
    };

    monQuakeData.getDetail = function(eventId, callback) {

        if (detailRecs[eventId]) { // Only pull if we don't already have detail data.
            callback(detailRecs[eventId]);
            return;
        }

        var detailUrlWithParams = monUiOptions.quakesUrl + '?eventid=' + eventId + '&format=geojson';
console.log('Earthquake API detail data URL:', detailUrlWithParams)
        $.getJSON(detailUrlWithParams, function(data) {
            var detailRec = processDetailAfterPull(data);
            detailRecs[eventId] = detailRec;
            callback(detailRec);
        });
    };

    monQuakeData.getQuakes = function (minLongIn, minLatIn, maxLongIn, maxLatIn, callback) {

        var urlParams = 
            '?maxlatitude=' + maxLatIn + '&minlatitude=' + minLatIn + '&maxlongitude=' + maxLongIn + 
            '&minlongitude=' + minLongIn + '&orderby=time' + 
            (monUiOptions.mapZoom < monUiOptions.zoomThreshold ? '&minmagnitude=3' : '');

        if (urlParams === urlParamsLastTry) {
            if (typeof callback === 'function' && callback()) { callback(); }
            return;
        }
        urlParamsLastTry = urlParams;
 
        monQuakeData.quakeRecs = [];
        monQuakeData.minMag = 0.0;
        monQuakeData.maxMag = 0.0;
        monQuakeData.minDepthKM = 0.0;
        monQuakeData.maxDepthKM = 0.0;
        monQuakeData.ageBuckets = {};

        $('#vscmmLoading').show();
        $('#vscmmLoading .loading-message').html(
                'Pulling Earthquake data from external data source and processing data.'
                );
        
        getQuakeDataFromFDNWS(minLongIn, minLatIn, maxLongIn, maxLatIn, function() {
        
            if (minLongIn < -180 || maxLongIn < -180) { // Crossing the date line, moving west. Do second data pull.

                if (minLongIn < -180 && maxLongIn > -180) {
                    maxLongIn = 180;
                    minLongIn = Math.abs(minLongIn) - 180;
                } else {
                    if (minLongIn < -180) { minLongIn = Math.abs(minLongIn) - 180; }
                    if (maxLongIn < -180) { maxLongIn = Math.abs(maxLongIn) - 180; }
                    if (maxLongIn < minLongIn) {
                        var tmp = maxLongIn;
                        maxLongIn = minLongIn;
                        minLongIn = tmp;
                    }
                }

                getQuakeDataFromFDNWS(minLongIn, minLatIn, maxLongIn, maxLatIn, function() {
                    refreshQuakeData();
                    while (monQuakeData.functionQueue.length > 0) { (monQuakeData.functionQueue.shift())(); }
                    if (typeof callback === 'function' && callback()) { callback(); }
                });
                
            } else {
            
                refreshQuakeData();
                while (monQuakeData.functionQueue.length > 0) { (monQuakeData.functionQueue.shift())(); }
                if (typeof callback === 'function' && callback()) { callback(); }
            }
        });
    };

}(window.monQuakeData = window.monQuakeData || {}, jQuery));
