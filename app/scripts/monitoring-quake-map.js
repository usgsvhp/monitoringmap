// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
/* global esri, dojo */

(function (monQuakeMap, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var quakeRec = {}, point = {}, colorRGB = [], symbol = {}, attribs = {}, quakeLayer = {}, quakeHoverLayer = {};

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    
    function renderQuakes() {

        monQuakeData.quakeRecs.sort(sortQuakesAsc); // Oldest first, to cause map to render newest last and on top.
        for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) {
            quakeRec = monQuakeData.quakeRecs[idx];
            if (quakeRec.isFiltered) { continue; }
            quakeLayer.add(makeQuakeGraphic(quakeRec));
        }
    }
    
    function makeQuakeGraphic(quakeRec) {
        
        point = new esri.geometry.Point(quakeRec.long, quakeRec.lat);
        point = esri.geometry.geographicToWebMercator(point);

        colorRGB = monUtils.getDepthColorRGB(parseFloat(quakeRec.depthKM));
        if (persistentOptions.get('quakeColorChoice') === 'time') {
            colorRGB = monUtils.getTimeColorRGB(quakeRec.ageDays);
        }
        
        colorRGB[3] = 0.85; // Add transparency

        symbol = new esri.symbol.SimpleMarkerSymbol(
            esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE,
            monUtils.getQuakePixelDiameter(quakeRec.mag),
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 0, 0]), 1),
            new dojo.Color(colorRGB)
        );

        var detailLink = '<a href="javascript:monQuakeUI.showDetail(\'' + quakeRec.eventId + '\');" ' + 
                            'title="Earthquake Report">Earthquake Report</a>';

        var depthString = monUtils.round(monUtils.kmToMi(quakeRec.depthKM), 1) + ' mi';
        if (persistentOptions.get('kmOrMi') === 'km') { depthString = quakeRec.depthKM + ' km'; }

        var timeString = quakeRec.quakeDateString + ' local';
        if (persistentOptions.get('utcOrLocal') === 'UTC') { timeString = quakeRec.quakeUTCString + ' UTC'; }

        attribs = {
            'mag': quakeRec.mag, 'timeString': timeString, 'depthString': depthString, 
            'href': detailLink, 'age': quakeRec.ageDays, 'eventId': quakeRec.eventId, 
            'graphicType': 'earthquake',
            'hoverText': 
                    '<b>Earthquake</b><br/>' + 
                    'Date: ' + timeString + '<br/>' +
                    'Magnitude: ' + quakeRec.mag + '<br/>' +
                    'Depth: ' + depthString + '<br/>' +
                    '<b><i>Click for additional information.</i></b>',
            'popupHtml' :
                    '<b>Earthquake</b><br/>' + 
                    'Date: ' + timeString + '<br/>' +
                    'Magnitude: ' + quakeRec.mag + '<br/>' +
                    'Depth: ' + depthString + '<br/><br/>' +
                    detailLink + ' | ' + '<a href="javascript:$(\'#vscmmMapMobilePopup\').hide();">CLOSE</a>'
            };

        return new esri.Graphic(point, symbol, attribs);
    }
    
    function makeHoverGraphic(quakeRec) {
        
        point = new esri.geometry.Point(quakeRec.long, quakeRec.lat);
        point = esri.geometry.geographicToWebMercator(point);

        colorRGB = monUtils.getDepthColorRGB(parseFloat(quakeRec.depthKM));
        if (persistentOptions.get('quakeColorChoice') === 'time') {
            colorRGB = monUtils.getTimeColorRGB(quakeRec.ageDays);
        }

        colorRGB[3] = 0.85; // Add transparency

        symbol = new esri.symbol.SimpleMarkerSymbol(
            esri.symbol.SimpleMarkerSymbol.STYLE_CROSS,
            monUtils.getQuakePixelDiameter(quakeRec.mag) * 4,
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_CROSS, new dojo.Color([0, 0, 0]), 1),
            new dojo.Color(colorRGB)
        );

        return new esri.Graphic(point, symbol);
    }
    
    function itemClicked(evt) {
        
        monMap.mapMouseOut();
        if (monMain.isDesktop) {
            monQuakeUI.showDetail(evt.graphic.attributes.eventId);
        } else {
            monQuakeUI.showLess(evt);
        }
    }

    function mapMouseOver(evt) {

        $('#vscmmMapMobilePopup').hide();
        monMap.mapMouseOver(evt, function() {
            monQuakeMap.activateQuake(evt.graphic.attributes.eventId);
        });
    }
    
    function mapMouseOut() {

        monQuakeMap.deactivateQuake();
    }
    
    function sortQuakesAsc(a, b) {
        
        if (parseFloat(a['unixTime']) < parseFloat(b['unixTime'])) { return -1; }
        if (parseFloat(a['unixTime']) > parseFloat(b['unixTime'])) { return 1; }
        return 0;
    }
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////
    
    monQuakeMap.renderQuakes = function () {
        
        quakeLayer.clear();
        renderQuakes();
    };
    
    monQuakeMap.activateQuake = function(eventId) {

        quakeRec = monQuakeData.getQuakeRec(eventId);

        quakeHoverLayer.clear();
        quakeHoverLayer.add(makeQuakeGraphic(quakeRec));
        quakeHoverLayer.add(makeHoverGraphic(quakeRec));

        monQuakeList.tablerowActive(eventId);
    };
    
    monQuakeMap.deactivateQuake = function() {

        monMap.mapMouseOut();
        quakeHoverLayer.clear();
        monQuakeList.tablerowInactive();
    };
    
    monQuakeMap.addMapLayers = function() {
        
        quakeHoverLayer = new esri.layers.GraphicsLayer();
        monMap.map.addLayer(quakeHoverLayer);

        quakeLayer = new esri.layers.GraphicsLayer();
        monMap.map.addLayer(quakeLayer);
        if (monMain.isDesktop) {
            dojo.connect(quakeLayer, 'onMouseOver', mapMouseOver);
            dojo.connect(quakeLayer, 'onMouseOut', mapMouseOut);
        }
        dojo.connect(quakeLayer, 'onClick', itemClicked);
    };

}(window.monQuakeMap = window.monQuakeMap || {}, jQuery));
